import argparse
import sys
import numpy
from pylab import *
from matplotlib import rc
from warnings import warn

rc('text', usetex=True)
rc('font', family='serif')
rc('font', serif='Times')

def load_data(files,saveto='consolidated.npz'):
	"""Loads the data from the .csv files into numpy arrays and saves for later use"""

	decisions = None
	last_header = None
	for f in files:
		fo = open(f, 'r')
		header = fo.readline()
		if last_header != None:
			if header != last_header:
				warn("headers don't match!")
				sys.exit(1)
		last_header = header
			
		a = numpy.loadtxt(fo, delimiter=',')
		
		if decisions == None:
			hl = [h.strip() for h in header.split(',')]
			if hl[0] != 'decision-cycle': warn("Expected column 'decision-cycle'")
			decisions = a[:,0]
			if hl[1] != 'prediction': warn("Expected column 'prediction'")
			predictions = a[:,1]
			if hl[2] != 'outcome': warn("Expected column 'outcome'")
			outcomes = a[:,2]
			if hl[7] != 'total-rulecount': warn("Expected column 'total-rulecount'")
			rules = a[:,7]
			if hl[8] != 'outcomedir': warn("Expected column 'outcomedir'")
			directions = a[:,8]
			if hl[9] != 'templates': warn("Expected column 'templates'")
			templates = a[:,9]
			if hl[10] != 'tdepth': warn("Expected column 'tdepth'")
			depth = a[:,10]
		else:
			if len(a) > len(decisions):
				predictions = numpy.column_stack([predictions, a[:len(decisions),1]])
				outcomes = numpy.column_stack([outcomes, a[:len(decisions),2]])
				rules = rules + a[:len(decisions),7]
				directions = numpy.column_stack([directions, a[:len(decisions),8]])
				templates = templates + a[:len(decisions),9]
				depth = depth + a[:len(decisions),10]
			else:
				decisions = decisions[:len(a)]
				predictions = numpy.column_stack([predictions[:len(decisions)], a[:,1]])
				outcomes = numpy.column_stack([outcomes[:len(decisions)], a[:,2]])
				rules = rules[:len(decisions)] + a[:,7]
				directions = numpy.column_stack([directions[:len(decisions)], a[:,8]])
				templates = templates[:len(decisions)] + a[:,9]
				depth = depth[:len(decisions)] + a[:,10]
				
	rules = rules / float(len(files))
	templates = templates / float(len(files))
	depth = depth / float(len(files))

	consolidated = {'decisions':decisions,
					'predictions':predictions,
					'outcomes':outcomes,
					'rules':rules,
					'directions':directions,
					'templates':templates,
					'depth':depth}
	numpy.savez(saveto, **consolidated)
	return consolidated

def smooth2(x, window_len, mode='flat'):

	smoothed = []
	for i in range(len(x)):
		start = max(i-window_len, 0)
		end = i
		smoothed.append( x[start:end+1].sum() / float(end-start+1) )
	return numpy.array(smoothed)


def generate_one_agent_learning_curve(agent_data):
	if type(agent_data) == str:
		agent_data = numpy.load(agent_data)

	(cp, cpy, cpn) = get_correct_predictions(agent_data)
	cp_width = float(cp.shape[1])

	# at each decision (row), sum up the columns that are correct
	print cp[0]
	print cp[0].sum()
	
	p_error = 1.0 - (cp.sum(axis=1)/cp_width)
	print len(agent_data['decisions'])
	print "Error @5K (+-5)", p_error[5000-5:5000+5] 
	print "Error @10K (+-5)", p_error[10000-5:10000+5]
	print "Mean:", p_error[10000-5:10000+5].mean()
	print "Error Between 10K-11K", p_error[10000:11000].mean()
	print "Error Between 10K-20K", p_error[10000:20000].mean()
	p_error_sm = smooth2(p_error, 100, 'flat')

	plot( agent_data['decisions'], p_error_sm, 'b-' )
	ylim([0,1])
	xlim([0,10000])
	title("Prediction Error")
	savefig('p_error_1a.pdf')
	clf()
	
def generate_one_agent_yn_learning_curve(agent_data):
	if type(agent_data) == str:
		agent_data = numpy.load(agent_data)

	(cp, cpy, cpn) = get_correct_predictions(agent_data)
	cpy_width = float(cpy.shape[1])
	cpn_width = float(cpn.shape[1])
	no = (agent_data['predictions'] == 0)
	yes = (agent_data['predictions'] == 1)

	# at each decision (row), sum up the columns that are correct
	print cp[0]
	print cp[0].sum()
	
	py_error = 1.0 - (cpy.sum(axis=1)/yes.sum(axis=1,dtype=numpy.float64))
	pn_error = 1.0 - (cpn.sum(axis=1)/no.sum(axis=1,dtype=numpy.float64))
	print len(agent_data['decisions'])
	print "Error @5K (+-5)", py_error[5000-5:5000+5] 
	print "Error @10K (+-5)", py_error[10000-5:10000+5]
	print "Mean:", py_error[10000-5:10000+5].mean()
	print "Error Between 10K-11K", py_error[10000:11000].mean()
	print "Error Between 10K-20K", py_error[10000:20000].mean()
	py_error_sm = smooth2(py_error, 200, 'flat')
	pn_error_sm = smooth2(pn_error, 200, 'flat')

	plot( agent_data['decisions'], py_error_sm, 'b-',
		  pn_error_sm, 'g-')
	ylim([0,1])
	xlim([0,10000])
	title("Prediction Error")
	savefig('p_error_1a_yn.pdf')
	clf()

def generate_rule_growth_curve(agent_data):
	if type(agent_data) == str:
		agent_data = numpy.load(agent_data)

	cla()
	(l1,l2) = plot(agent_data['decisions'], agent_data['rules'], 'b-',
		 agent_data['templates'], 'g-')
	xlim([0,10000])


	ax = gca()
	ax.set_aspect(15)
	for tick in ax.xaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
	for tick in ax.yaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
		
	xlabel("Decisions",fontsize=20)
	ylabel("Number of Rules",fontsize=20)
	legend( (l1, l2), ('Total Rules', 'Templates'), 'upper left')
	savefig('rule_growth.pdf')
	clf()
	
def generate_two_agent_learning_curve(a1d, a2d, a1name,a2name):
	a_dict = {}
	if type(a1d) == str:
		a1d = numpy.load(a1d)
	if type(a2d) == str:
		a2d = numpy.load(a2d)
		
	for agent_data,agent_name in [(a1d,a1name), (a2d,a2name)]:

		(cp, cpy, cpn) = get_correct_predictions(agent_data)
		cp_width = float(cp.shape[1])

		# at each decision (row), sum up the columns that are correct
		print cp[0]
		print cp[0].sum()

		p_error = 1.0 - (cp.sum(axis=1)/cp_width)
		print p_error[0]
		print agent_name
		print len(agent_data['decisions'])
		print "Error @5K (+-5)", p_error[5000-5:5000+5] 
		print "Error @10K (+-5)", p_error[10000-5:10000+5]
		print "Mean:", p_error[10000-5:10000+5].mean()
		print "Error Between 10K-11K", p_error[10000:11000].mean()
		print "Error Between 10K-20K", p_error[10000:20000].mean()
		p_error_sm = smooth2(p_error, 100, 'flat')
		a_dict[(agent_name,'er')] = p_error_sm
		print p_error_sm[0:10]
		
		#no = (predictions == 0)
		#yes = (predictions == 1)
		up = (agent_data['directions'] == 2)
		left = (agent_data['directions'] == 0)

		correct_up = np.ma.array(cp, mask=(agent_data['directions'] != 2))
		correct_left = np.ma.array(cp, mask=(agent_data['directions'] != 0))
		print "Does correct == correct_yes + correct_no?"
		cs = cp.sum(axis=1)
		cy = cpy.sum(axis=1)
		cn = cpn.sum(axis=1)
		
		cyu_error = (up.sum(axis=1) - correct_up.sum(axis=1))/up.sum(axis=1,dtype=np.float64)
		cyl_error = (left.sum(axis=1) - correct_left.sum(axis=1))/left.sum(axis=1,dtype=np.float64)

		cyu_error_sm = smooth(cyu_error, 111, 'flat')
		cyl_error_sm = smooth(cyl_error, 111, 'flat')
		
		for i in range(len(cs)):
			if cs[i] != cy[i] + cn[i]:
				print "yikes!"
				sys.exit(1)

		a_dict[(agent_name,'yuer')] = cyu_error_sm
		a_dict[(agent_name,'yler')] = cyl_error_sm
		
	print a1d['decisions'].shape
	print a_dict[(a1name,'er')].shape
	print a_dict[(a2name,'er')].shape
	l1, l2 = plot( a1d['decisions'], a_dict[(a1name,'er')], 'b-',
				   a_dict[(a2name, 'er')], 'g-' )
	ylim([0,.5])
	xlim([0,10000])

	ax = gca()
	for tick in ax.xaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
	for tick in ax.yaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
		
	xlabel("Decisions",fontsize=20)
	ylabel("Prediction Error",fontsize=20)
	legend( (l1, l2), (a1name, a2name))
	savefig('p_error_2a.pdf')
	clf()

	l1,l2,l3,l4 = plot( a1d['decisions'], a_dict[(a1name,'yuer')], 'b--',
						a_dict[(a1name,'yler')], 'b-',
		                a_dict[(a2name, 'yuer')], 'g--',
						a_dict[(a2name,'yler')], 'g-',
		)
	ylim([0,1])
	xlim([0,10000])

	ax = gca()
	for tick in ax.xaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
	for tick in ax.yaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
	xlabel("Decisions",fontsize=20)
	ylabel("Prediction Error",fontsize=20)
	legend( (l1, l2,l3,l4), (a1name +" (Up)",a1name +" (Left)", a2name+" (Up)",a2name+" (Left)"))


	savefig('p_error_2a_lu.pdf')
	clf()

def get_correct_predictions(data):
	correct_predictions = (data['predictions'] == data['outcomes'])
	# find the predictions that were 'yes'
	#	correct_predict_yes = np.ma.array((predictions==1),
	#                                     mask=np.logical_not(correct_predictions))

	correct_predict_no = np.ma.array(correct_predictions,
									 mask=(data['predictions'] == 1))

	correct_predict_yes = np.ma.array(correct_predictions,
									  mask=(data['predictions'] == 0))
	
	return (correct_predictions, correct_predict_yes, correct_predict_no)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('files', nargs='*',
						help="a list of the files to consolidate")
	parser.add_argument('--cname', default="consolidated.npz",
						help="the name of the .npz file to store")

	args = parser.parse_args()

	arrayz = load_data(args.files, args.cname)
	sys.exit(1)
	(correct, correct_yes, correct_no) = get_correct_predictions(predictions, outcomes)

	pw = predictions.shape[1]

	no = (predictions == 0)
	yes = (predictions == 1)
	up = (directions == 2)
	left = (directions == 0)

	print "Here"
	print correct_yes.shape
	print predictions.shape
	print outcomes.shape
	print directions.shape
	correct_up = np.ma.array(correct, mask=(directions != 2))
	correct_l = np.ma.array(correct, mask=(directions != 0))
	print "Does correct == correct_yes + correct_no?"
	cc = np.logical_or(correct_yes, correct_no)
	cs = correct.sum(axis=1)
	cy = correct_yes.sum(axis=1)
	cn = correct_no.sum(axis=1)
	cyu_error = (up.sum(axis=1) - correct_up.sum(axis=1))/up.sum(axis=1,dtype=np.float64)
	cyl_error = (left.sum(axis=1) - correct_l.sum(axis=1))/left.sum(axis=1,dtype=np.float64)
	for i in range(len(cs)):
		if cs[i] != cy[i] + cn[i]:
			print "yikes!"
			sys.exit(1)
	print cyu_error[0:10]
	p_yes_error = 1.0 - (correct_yes.sum(axis=1)/predictions.sum(axis=1))
	p_no_error = 1.0 - (correct_no.sum(axis=1)/(pw - predictions.sum(axis=1)))
	correct_width = float(correct.shape[1])
	p_error = 1.0 - (correct.sum(axis=1)/correct_width)
	print len(decisions)
	print "Error @5K (+-5)", p_error[5000-5:5000+5] 
	print "Error @10K (+-5)", p_error[10000-5:10000+5]
	print "Mean:", p_error[10000-5:10000+5].mean()
	print "Error Between 10K-11K", p_error[10000:11000].mean()
	print "Error Between 10K-20K", p_error[10000:20000].mean()
	sm = smooth(p_error, 55, 'flat')
	su = smooth(cyu_error, 55, 'flat')
	sl = smooth(cyl_error, 111, 'flat')
	print cyu_error[:50]
	print cyl_error[:100]
	print correct_l[:20]
	print left[:20].sum(axis=1)
	print correct_l[:20].sum(axis=1)
	print len(sm)
	print len(p_error)
	plot( decisions, su, 'r-', sl, 'g-' )
	ylim([0,.7])
	xlim([0,10000])
	title("Error")
	savefig('error.png')
	#	show()
