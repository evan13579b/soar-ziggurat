#!/usr/bin/env python2.7


#
# flip_predict.py
# An implementation of the Ziggurat.FlipPredictEnvironment
#
#  Based on the example python soar environment
#  http://code.google.com/p/soar/wiki/ExampleCode_SoarExp
#

from itertools import product
from os import environ as env, fsync
from subprocess import call, check_output, CalledProcessError, STDOUT
import re
import sys
import random
import time
import os



if "DYLD_LIBRARY_PATH" in env:
	LIB_PATH = env["DYLD_LIBRARY_PATH"]
elif "LD_LIBRARY_PATH" in env:
	LIB_PATH = env["LD_LIBRARY_PATH"]
else:
	print "Soar LIBRARY_PATH environment variable not set; quitting" 
	exit(1)
sys.path.append(LIB_PATH)
import Python_sml_ClientInterface as sml

# low level Soar functions

def create_kernel():
	kernel = sml.Kernel.CreateKernelInCurrentThread()
	if not kernel or kernel.HadError():
		print "Error creating kernel: " + kernel.GetLastErrorDescription()
		exit(1)
	return kernel

def create_agent(kernel, name):
	agent = kernel.CreateAgent("flip")
	if not agent:
		print "Error creating agent: " + kernel.GetLastErrorDescription()
		exit(1)
	return agent

# mid-level framework


def cli(agent):
	cmd = raw_input("soar> ")
	cmd_parts = cmd.split()
	while cmd_parts[0] not in ("exit", "quit"):
		if cmd_parts[0] == "slave":
			slave(cmd, cmd_parts)
		elif cmd:
			print agent.ExecuteCommandLine(cmd).strip()
		cmd = raw_input("soar> ")
		cmd_parts = cmd.split()


def slave(cmd, cmd_parts):
	if cmd_parts[0] == "slave":
		log_file = sys.stdout
		decisions = None
		print "Entering slave mode:"
		if len(cmd_parts) > 1:
			log_filename = cmd_parts[1]
			print "  - log file '%s'...." % log_filename
			log_file = open(log_filename, 'w')
		if len(cmd_parts) > 2:
			decisions = int(cmd_parts[2])
			print "  - will exit slave mode after %d decisions" % decisions
		else:
			print "  - will remain in slave mode indefinitely...kill process to exit."
	
		print "  waiting for commands from an externally connected sml process..."

		i = 0
		spinner = ['-','/','|','\\']
		dc_count = agent.GetDecisionCycleCounter()
		if decisions != None:
			decisions = dc_count + decisions
		while True:
			if dc_count == decisions:
				print "Reached %d decisions. leaving slave mode" % decisions
				break
			
			kernel.CheckForIncomingCommands()
			if agent.GetDecisionCycleCounter() > dc_count + 1:
				print "WARNING: stepped more than 1 dc"
				#	elif agent.GetDecisionCycleCounter() == dc_count + 1:
				#print agent.ExecuteCommandLine("print --depth 4 --internal i1")				
			dc_count = agent.GetDecisionCycleCounter()
			
			if dc_count % 10 == 1:
				process_rl_cmd( agent.ExecuteCommandLine("print --rl"), log_file )

				
			sys.stdout.write(spinner[i])
			sys.stdout.flush()
			#time.sleep(.1)
			sys.stdout.write('\b')
			i += 1
			i %= len(spinner)



def find_rule_name_alias( name, memo ):
	"""This function tries to generate easily recognizeable alternate
	names (aliases) for soar rl template rule instantiations.

	Note that this implementation is *entirely* domain dependent"""

	# if we've already created an alias, return it
	if name in memo:
		return memo[name]

	# if the rule doesn't start with rl*, its not an rl template rule
	# so no alias is needed...
	if not name.startswith("rl*"):
		memo[name] = name
		return name
	
	# domain dependent alias naming
	name_match = re.match("(rl\*prefer\*predict-.*\*direction\*)\d+", name)
	if name_match:
		internal = agent.ExecuteCommandLine("print --internal %s" % name)
		print internal
		c_match = re.search("\^dir (.+)\)", internal)
		# the first part of the rule name + the direction it's using
		new_name = name_match.group(1) + c_match.group(1)
		memo[name] = new_name
		return new_name

	name_match = re.match("(rl\*prefer\*predict-.*\*direction\*zigid\*)\d+", name)
	if name_match:
		internal = agent.ExecuteCommandLine("print --internal %s" % name)
		print internal
		c_match = re.search("\^dir (.+)\)", internal)
		c2_match = re.search("\^zigg-state-id (.+)\)", internal)
		# the first part of the rule name + the direction it's using
		new_name = name_match.group(1) + c_match.group(1) + c2_match.group(1)
		memo[name] = new_name
		return new_name

	memo[name] = name
	return name
		
def process_rl_cmd(rlstring, stream=sys.stdout, _rule_memo={}, _rule_name_memo={}):
	"""Process the string returned by the 'print --rl' command

	_rule_memo and _rule_name_memo should *NOT* be supplied by the caller,
	these are internally used memoization dictionaries
	"""
	
	rules = []
	for l in rlstring.split('\n'):
		elements = l.split()
		if len(elements) > 0:
			if len(elements) != 5:
				print "WARNING: unexpected number of elements!"
			else:
				(rule_name, n, q, mean, variance) = elements
				rules.append((rule_name, float(n), float(q), float(mean), float(variance)))

	s_rules = [(r[2], r[3], r) for r in rules]
	s_rules.sort()
	current_dc = agent.GetDecisionCycleCounter()
	print >>stream, "Sorted rules @ decision ", current_dc
	print >>stream, "%-60s %9s %9s %9s %9s" % ("rule name", "n", "q", "mean", "var")
	print >>stream, "-"*90
	has_new_rules = False
	for r in s_rules:
		find_rule_name_alias( r[2][0], _rule_name_memo )
		print >>stream, "%-60s %9.5f %9.5f %9.5f %9.5f" % ((_rule_name_memo[r[2][0]],)+r[2][1:])
		# keep track of the rules we've seen and when we've first seen them
		if not r[2][0] in _rule_memo:
			_rule_memo[r[2][0]] = current_dc
			has_new_rules = True

	print >>stream, "-"*90
	if has_new_rules:
		print >>stream, "New rules:"
		for r in _rule_memo:
			if _rule_memo[r] == current_dc:
				print "rule alias: '%s'" % _rule_name_memo[r]
				print >>stream, agent.ExecuteCommandLine("print %s" % r)
				print ""
		print >>stream, "-"*90
		
# IO

def parse_output_commands(agent, structure):
	commands = {}
	mapping = {}
	for cmd in range(0, agent.GetNumberCommands()):
		error = False
		command = agent.GetCommand(cmd)
		cmd_name = command.GetCommandName()
		if cmd_name in structure:
			parameters = {}
			for param_name in structure[cmd_name]:
				param_value = command.GetParameterValue(param_name)
				if param_value:
					parameters[param_name] = param_value
			if not error:
				commands[cmd_name] = parameters
				mapping[cmd_name] = command
		else:
			error = True
		if error:
			command.AddStatusError()
	return commands, mapping


# callback registry

def register_print_callback(kernel, agent, function, user_data=None):
	agent.RegisterForPrintEvent(sml.smlEVENT_PRINT, function, user_data)

def register_output_callback(kernel, agent, function, user_data=None):
	agent.RegisterForRunEvent(sml.smlEVENT_AFTER_OUTPUT_PHASE, function, user_data)

def register_output_change_callback(kernel, agent, function, user_data=None):
	kernel.RegisterForUpdateEvent(sml.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, function, user_data)

def register_destruction_callback(kernel, agent, function, user_data=None):
	agent.RegisterForRunEvent(sml.smlEVENT_AFTER_HALTED, function, user_data)

# callback functions

def callback_print_message(mid, user_data, agent, message):
	print message.strip()


_WORLD_STATE = None
STATE_1 = "State ONE"
STATE_2 = "State TWO"
REWARD_WME = None
DIR_WME = None
_DIR = None
_DIR_MAP = {0:'L', 1:'R', 2:'U'}

# Added by Evan: We need see to be part of the episodes so we need to send this to soar's output-link
SEE_WME = None

decisionCycleCounter=0;

predictYesResults=[]
predictNoResults=[]
overallResults=[]
dirLeftResults=[]
dirRightResults=[]
dirUpResults=[]

RESULTS_FILE="results"

def calculateErrorAverages(predicted,actual,n):
	global predictYesResults,predictNoResults
	
	if predicted==1:
		if actual==1:
			predictYesResults.append(0)
		else:
			predictYesResults.append(1)
	else:
		if actual==0:
			predictNoResults.append(0)
		else:
			predictNoResults.append(1)

	predict_yes_n=min(n,len(predictYesResults))
	predict_yes_correct=sum(predictYesResults[-predict_yes_n:])
	yes_average=float(predict_yes_correct)/predict_yes_n if predict_yes_n>0 else 0
	predict_no_n=min(n,len(predictNoResults))
	predict_no_correct=sum(predictNoResults[-predict_no_n:])
	no_average=float(predict_no_correct)/predict_no_n if predict_no_n>0  else 0
	return {"predict-yes":yes_average,"predict-no":no_average}

def getNumberOfRules(agent):
	#return {"predict-yes":0,"predict-no":0,"total":0,"templates":0, "depth":0}
	rlRules=agent.ExecuteCommandLine("print --rl")
	nPredictYes=rlRules.count("predict-yes")
	nPredictNo=rlRules.count("predict-no")
	total=nPredictYes+nPredictNo
	templates=agent.ExecuteCommandLine("print --template")
	ntemplates = 0
	# a poor mans way of charting the template depth is to count the number of H's
	# in the template and subtract 1
	depth = 0
	for l in templates.split('\n'):
		if l.strip() != "": ntemplates += 1
		if l.count("H")-1 > depth:
			depth = l.count("H")-1
	return {"predict-yes":nPredictYes,"predict-no":nPredictNo,"total":total,"templates":ntemplates, "depth":depth}

def logResults(errorAverages,ruleCounts,prediction,actual,outcomedir='?'):
	global resultsFile,decisionCycleCounter
	resultsFile.writelines([str(decisionCycleCounter)+","+str(prediction)+","+str(actual)+","+str(errorAverages["predict-yes"])+","+str(errorAverages["predict-no"])+","+str(ruleCounts["predict-yes"])+","+str(ruleCounts["predict-no"])+","+str(ruleCounts["total"])+","+str(outcomedir)+","+str(ruleCounts["templates"])+","+str(ruleCounts["depth"])+"\n"])
	resultsFile.flush()
	decisionCycleCounter+=1

def pickDirectionRandomly():
	return random.choice(_DIR_MAP.keys())
	
	#dir=random.choice([0,1,2,2])
	print "dir: dir is"+str(dir)
	return dir


currentTime=0
timeLogFile=open("timelogfile","w")
counter=0
counterLimit=10000
lastRuleCount=None
def callback_output(mid, user_data, agent, *args):
	global _WORLD_STATE, _DIR_MAP, _DIR,SEE_WME,currentTime,timeLogFile,counter,counterLimit,lastRuleCount
	
	previousDirection=_DIR

	counter+=1

	timeLogFile.write("time at start of callback is "+str(time.time())+"\n");
	

	assert agent.GetNumberCommands() <= 1

	expectedOutcome = -1
	for n in range(agent.GetNumberCommands()):
		cid = agent.GetCommand(n)

		assert cid.GetCommandName() in ['predict-yes', 'predict-no']

		if cid.GetCommandName() == "predict-yes": expectedOutcome = 1
		if cid.GetCommandName() == "predict-no": expectedOutcome = 0

		cid.AddStatusComplete()
		agent.Commit()
		agent.ClearOutputLinkChanges()
		print "ENV: Agent did:", cid.GetCommandName(), "for direction", _DIR_MAP[_DIR], "in state", _WORLD_STATE 


	if expectedOutcome == -1:
		raise "This is a problem ;)"

	print_state(expectedOutcome)
	actualOutcome = 0
	if _WORLD_STATE == STATE_1 and _DIR_MAP[_DIR] == 'R':
		_WORLD_STATE = STATE_2
		actualOutcome = 1
	elif _WORLD_STATE == STATE_2 and _DIR_MAP[_DIR] == 'L':
		_WORLD_STATE = STATE_1
		actualOutcome = 1

	print "ENV: Next state will be:", _WORLD_STATE, "see will be:", actualOutcome, "prediction was correct?", (actualOutcome==expectedOutcome)
	outcomedir = _DIR
	_DIR = pickDirectionRandomly()
	agent.Update(DIR_WME, _DIR_MAP[_DIR])

	currentError=0 if actualOutcome==expectedOutcome else 1

	if counter>counterLimit:
		if lastRuleCount==None:
			lastRuleCount=getNumberOfRules(agent)
		numberOfRules=lastRuleCount
	else:
		numberOfRules=getNumberOfRules(agent)	
	
	logResults(calculateErrorAverages(previousDirection,expectedOutcome,actualOutcome,100),
                   numberOfRules,previousDirection,expectedOutcome,actualOutcome)


	# Added by Evan: We need see to be part of the episodes so we need to send this to soar so it can send this to ziggurat
	agent.Update(SEE_WME, actualOutcome)

	if expectedOutcome == actualOutcome:
		agent.Update(REWARD_WME, 1)
		print "predict error 0"
	else:
		agent.Update(REWARD_WME, 0)
		print "predict error 1"
	
	agent.Commit()
	timeLogFile.write("time at   end of callback is "+str(time.time())+"\n");
	timeLogFile.flush()

def print_state(cmd):
	global _WORLD_STATE, _DIR_MAP, _DIR

	print "In ", _WORLD_STATE, "moving", _DIR_MAP[_DIR]
	


		
		

if __name__ == "__main__":        
        resultsFile=open(RESULTS_FILE,"w")
        resultsFile.writelines(["decision-cycle,"+
				"direction,"+
				"prediction,"+
				"outcome,"+
				"predict-yes error,"+
				"predict-no error,"+
				"dir left error,"+
				"dir right error,"+
				"dir up error,"+
				"total error,"+
				"predict-yes rule count,"+
				"predict-no rule count,"+
				"total rule count,"+
				"number of templates\n"])
        resultsFile.flush()
	
	kernel = create_kernel()
	agent = create_agent(kernel, "flip")
	register_print_callback(kernel, agent, callback_print_message, None)
	register_output_callback(kernel, agent, callback_output, None)
	
	_WORLD_STATE = STATE_1
	_DIR = pickDirectionRandomly()

	il = agent.GetInputLink()
	REWARD_WME = il.CreateIntWME( "reward", 0)
	DIR_WME = il.CreateStringWME( "dir", _DIR_MAP[_DIR] )
	
	# Added by Evan: We need see to be part of the episodes so we need to send this to soar so it can send this to ziggurat
	SEE_WME = il.CreateIntWME( "see", 0 )	

	print "Python-Soar Flip environment."
	print "To accept commands from an external sml process, you'll need to"
	print "type 'slave <log file> <n decisons>' at the prompt..."
	#  Python seems sensitive to threading as discussed in the Tcl entry
	#  in the sml documentation... 'Events in Tcl'
	#  http://code.google.com/p/soar/wiki/SMLQuickStartGuide
	if sys.args.sourcefiles:
		for source in args.sourcefiles:
			print "sourcing " + source
			print agent.ExecuteCommandLine("source " + source)
	else:
		print "sourcing 'flip_predict.soar'"
		if os.path.exists( "resources" ):
			print agent.ExecuteCommandLine("source ./resources/flip_predict.soar")
		elif os.path.exists( "../resources" ):
			print agent.ExecuteCommandLine("source ../resources/flip_predict.soar")
			
	cli(agent)
	kernel.DestroyAgent(agent)
	kernel.Shutdown()
	del kernel
