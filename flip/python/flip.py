#!/usr/bin/env python2.7


#
# flip.py
#  Based on the example python soar environment
#  http://code.google.com/p/soar/wiki/ExampleCode_SoarExp
#

from itertools import product
from os import environ as env, fsync
from subprocess import call, check_output, CalledProcessError, STDOUT
import re
import sys
import time

if "DYLD_LIBRARY_PATH" in env:
	LIB_PATH = env["DYLD_LIBRARY_PATH"]
elif "LD_LIBRARY_PATH" in env:
	LIB_PATH = env["LD_LIBRARY_PATH"]
else:
	print("Soar LIBRARY_PATH environment variable not set; quitting")
	exit(1)
sys.path.append(LIB_PATH)
import Python_sml_ClientInterface as sml

# low level Soar functions

def create_kernel():
	kernel = sml.Kernel.CreateKernelInCurrentThread()
	if not kernel or kernel.HadError():
		print("Error creating kernel: " + kernel.GetLastErrorDescription())
		exit(1)
	return kernel

def create_agent(kernel, name):
	agent = kernel.CreateAgent("flip")
	if not agent:
		print("Error creating agent: " + kernel.GetLastErrorDescription())
		exit(1)
	return agent

# mid-level framework

def cli(agent):
	cmd = raw_input("soar> ")
	while cmd not in ("exit", "quit", "slave"):
		if cmd:
			print(agent.ExecuteCommandLine(cmd).strip())
		cmd = raw_input("soar> ")

	if cmd == "slave":
		print "Entering slave mode.... you'll need to kill this process to end."
		print "... waiting for commands from an externally connected sml process..."

		i = 0
		spinner = ['-','/','|','\\']
		while True:
			kernel.CheckForIncomingCommands()
			sys.stdout.write(spinner[i])
			sys.stdout.flush()
			time.sleep(.2)
			sys.stdout.write('\b')
			i += 1
			i %= len(spinner)

# IO

def parse_output_commands(agent, structure):
	commands = {}
	mapping = {}
	for cmd in range(0, agent.GetNumberCommands()):
		error = False
		command = agent.GetCommand(cmd)
		cmd_name = command.GetCommandName()
		if cmd_name in structure:
			parameters = {}
			for param_name in structure[cmd_name]:
				param_value = command.GetParameterValue(param_name)
				if param_value:
					parameters[param_name] = param_value
			if not error:
				commands[cmd_name] = parameters
				mapping[cmd_name] = command
		else:
			error = True
		if error:
			command.AddStatusError()
	return commands, mapping


# callback registry

def register_print_callback(kernel, agent, function, user_data=None):
	agent.RegisterForPrintEvent(sml.smlEVENT_PRINT, function, user_data)

def register_output_callback(kernel, agent, function, user_data=None):
	agent.RegisterForRunEvent(sml.smlEVENT_AFTER_OUTPUT_PHASE, function, user_data)

def register_output_change_callback(kernel, agent, function, user_data=None):
	kernel.RegisterForUpdateEvent(sml.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, function, user_data)

def register_destruction_callback(kernel, agent, function, user_data=None):
	agent.RegisterForRunEvent(sml.smlEVENT_AFTER_HALTED, function, user_data)

# callback functions

def callback_print_message(mid, user_data, agent, message):
	print(message.strip())


WORLD_STATE = "A"
SEE = None

def callback_output(mid, user_data, agent, *args):
	global WORLD_STATE, SEE
	
	assert agent.GetNumberCommands() <= 1
		
	for n in range(agent.GetNumberCommands()):
	    cid = agent.GetCommand(n)

	    assert cid.GetCommandName() in ['up', 'left', 'right']

	    if cid.GetCommandName() == "left" and WORLD_STATE == "B":
		    WORLD_STATE = "A"
		    agent.Update(SEE, 1)

	    elif cid.GetCommandName() == "right" and WORLD_STATE == "A":
		    WORLD_STATE = "B"
		    agent.Update(SEE, 1)

	    else:
		    agent.Update(SEE, 0)

	    cid.AddStatusComplete()
	    agent.Commit()
	    agent.ClearOutputLinkChanges()
	    
	    print "You did:", cid.GetCommandName(), "and are now in state", WORLD_STATE


	    
	    

if __name__ == "__main__":
	
	kernel = create_kernel()
	agent = create_agent(kernel, "agent")
	register_print_callback(kernel, agent, callback_print_message, None)
	register_output_callback(kernel, agent, callback_output, None)

	il = agent.GetInputLink()
	SEE = il.CreateIntWME( "see", 0)

	print "Python-Soar Flip environment."
	print "To accept commands from an external sml process, you'll need to"
	print "type 'slave' at the prompt..."
	#  Python seems sensitive to threading as discussed in the Tcl entry
	#  in the sml documentation... 'Events in Tcl'
	#  http://code.google.com/p/soar/wiki/SMLQuickStartGuide
	for source in sys.argv[1:]:
		print(agent.ExecuteCommandLine("source " + source))
	else:
		print(agent.ExecuteCommandLine("source ../resources/flip.soar"))
		
	cli(agent)
	kernel.DestroyAgent(agent)
	kernel.Shutdown()
	del kernel
