import sys

def parseLine(line):
    	return [float(part) for part in line.split(",")]

def combineLines(lines):
    n=float(len(lines))
    lineSize=len(lines[0])
    combination=[0]*lineSize
    for i in range(0,lineSize):
        currentSum=0
        for line in lines:
            currentSum+=line[i]
        average=currentSum/n
        combination[i]=average
    return combination
    

def writeDataLine(outputFile,line):
    for part in line[:-1]:
        outputFile.write(str(part)+",")
    outputFile.write(str(line[-1]))
    outputFile.write("\n")
    outputFile.flush()

args=sys.argv

outputFilename=args[1]
inputFilenames=args[2:]

outputFile=open(outputFilename,"w")

inputFiles=[]
for filename in inputFilenames:
    inputFiles.append(open(filename,"r"))


fileLinesArray=[]
for inputFile in inputFiles:
    fileLinesArray.append(inputFile.readlines())

outputFile.write(fileLinesArray[0][0])

lengths=[len(fileLines) for fileLines in fileLinesArray]
minLength=min(lengths)


for i in range(1,minLength):
    lines=[parseLine(fileLines[i]) for fileLines in fileLinesArray]
    writeDataLine(outputFile,combineLines(lines))

outputFile.close()
for inputFile in inputFiles:
    inputFile.close()
