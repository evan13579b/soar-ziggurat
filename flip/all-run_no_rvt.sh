#!/bin/bash

echo > results

filename=$1
numberOfCycles=$2

echo slave | DYLD_LIBRARY_PATH=../Soar-Suite/out/ python python/flip_predict.py resources/flip_predict_no_rvt.soar  > environmentOutput &
envPID=$!
echo "environment pid is" $envPID
cd ../ziggurat

sleep 2

DYLD_LIBRARY_PATH=../Soar-Suite/out LD_LIBRARY_PATH=../Soar-Suite/out java -cp bin/:../Soar-Suite/out/java/sml.jar:../Soar-suite/out/java/soar-smljava.jar Ziggurat.MCP env=soar > /dev/null &
ziggPID=$!
echo "ziggPID is " $ziggPID

cd ../flip

true=1
false=0
notDone=$true

killProgramsAndExit()
{
	kill $ziggPID
	kill $envPID

	cp results $filename
	exit
}

trap killProgramsAndExit SIGINT

while [ $notDone -eq $true ]
do
lastLine=`tail -1 results`
echo $lastLine
currentCycle=`echo $lastLine | sed  's/\([0-9]*\).*/\1/'`
#echo "currentCycle is $currentCycle"
sleep 1

if [ $currentCycle -ge $numberOfCycles ]
then
	notDone=$false
	killProgramsAndExit
fi
done
