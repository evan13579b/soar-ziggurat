package soar;
import java.util.Random;
import java.util.Scanner;

import sml.Identifier;
import sml.IntElement;
import sml.Kernel;
import sml.Agent;
import sml.StringElement;
import sml.smlPrintEventId;
import sml.smlUpdateEventId;
import sml.Agent.PrintEventInterface;
import sml.Kernel.UpdateEventInterface;


/**
 * A FlipPredict Environment for Soar.
 * 
 * This is built to work identically to the FlipPredictEnvironment for Ziggurat. I've tried
 * to clean up the terminology a bit.
 * 
 * Here's how it's stated in the Ziggurat FlipPredictEnvironment:
 *   
 *   the agent tries to predict the reward that will be received for a random action.  In
 *   effect, the Left/Right/Up action becomes a feature of the state space and the
 *   agent's actions become Reward=0 or Reward=1.
 * 
 * I've tried to stay as close as possible to the Ziggurat.FlipPredictEnvironment implementation
 * 
 * @author wallaces
 *
 */
public class FlipPredictEnvironment implements Runnable, UpdateEventInterface, PrintEventInterface {
	
	// Define possible states
	private enum State { STATE_1, STATE_2 };
	private State currentState;
	private int currentDirection;

	// Define command related variables
	private static final int DIR_LEFT 		 = 0;
	private static final int DIR_RIGHT 	 = 1;
	private static final int DIR_UP 		 = 2;
	private static final int NUM_DIRECTIONS = 3;
	private static final String DIR_STRINGS[] = {"left", "right", "up"};
	
	Random randGen = new Random(1);

	private String environment;
	private Kernel kernel;
	private Agent agent;
	private IntElement rewardWme;
	private StringElement dirWme;

	
	public static void main(String []args) {
		System.out.println("FlipPredict Environment!");
		FlipPredictEnvironment fe;
		if (args.length > 1)
			fe = new FlipPredictEnvironment(args[1]);
		else
			fe = new FlipPredictEnvironment("flip_predict");
			
		fe.run();
	}

	public FlipPredictEnvironment(String envname) {
		environment = envname;
		
		try {
			// create our Soar kernel
			kernel = Kernel.CreateKernelInNewThread();
		} catch (Exception e) {
			System.out.println("Exception while creating kernel: " + e.getMessage());
			System.exit(1);
		}

		if (kernel.HadError())
		{
			System.out.println("Error creating kernel: " + kernel.GetLastErrorDescription()) ;
			System.exit(1);
		}

		kernel.SetAutoCommit(false);

		agent = kernel.CreateAgent("flip");

		// Register for the event we'll use to update the world
		// We update the environment when this event fires.  This allows us to either run the environment directly or from a debugger and get correct behavior
		kernel.RegisterForUpdateEvent(smlUpdateEventId.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, this, null) ;
		agent.RegisterForPrintEvent(smlPrintEventId.smlEVENT_PRINT, this, null);


		// initialize ala Ziggurat.FlipPredictEnvironment
		currentState = State.STATE_1;
		currentDirection = randGen.nextInt(NUM_DIRECTIONS);
		System.out.println("CD is " + currentDirection);
		Identifier il = agent.GetInputLink();
		rewardWme = il.CreateIntWME("reward", 0); 
		dirWme = il.CreateStringWME("dir", DIR_STRINGS[currentDirection]);		
		agent.Commit();

		
		System.out.println("Looking for '" + environment + ".soar'...");
		System.out.println(agent.ExecuteCommandLine("source resources/"+
				environment + ".soar").trim());

	}

	public void cli() {
		Scanner scan_in = new Scanner(System.in);
		System.out.print(environment+"-soar>");
		String cmd = scan_in.nextLine().trim();
		while( !cmd.equals("quit") && !cmd.equals("exit") ) {
			if (!cmd.equals(""))
				System.out.println(agent.ExecuteCommandLine(cmd).trim());
			System.out.print(environment+"-soar>");
			cmd = scan_in.nextLine().trim();
		}

	}

	
	@Override
	public void updateEventHandler(int arg0, Object arg1, Kernel arg2,
			int arg3) {

		// First, get the agent's prediction
		if (agent.GetNumberCommands() > 1) {
			System.err.println("Too many commands!");
		}
		Identifier cid;
		int expOutcome = -1;
		for(int i = 0; i < agent.GetNumberCommands(); i++) {

			cid = agent.GetCommand(i);
			if (cid.GetCommandName().equals("predict-yes")) {
				expOutcome = 1;
			}
			else if (cid.GetCommandName().equals("predict-no")) {
				expOutcome = 0;
			}
			else {
				System.err.println("Unexpected command! '" + cid.GetCommandName() + "'");
			}
			cid.AddStatusComplete();
			agent.Commit();
			agent.ClearOutputLinkChanges();
		}
		if (expOutcome == -1) {
			System.err.println("No Commands / Unexpected Command?");
			System.exit(1);
		}
		
		printState(expOutcome);
			
		// from Ziggurat.FilPredictEnvironment
		int actualOutcome = 0;
		if ( (this.currentState == State.STATE_1) &&
				(this.currentDirection == DIR_RIGHT)) {
			this.currentState = State.STATE_2;
			actualOutcome = 1;
		}
		else if ( (this.currentState == State.STATE_2) &&
				(this.currentDirection == DIR_LEFT)) {
			this.currentState = State.STATE_1;
			actualOutcome = 1;
		}
		
		this.currentDirection = randGen.nextInt(NUM_DIRECTIONS);
		agent.Update(dirWme, DIR_STRINGS[this.currentDirection]);
		if (expOutcome == actualOutcome) {
			agent.Update(rewardWme, 1);
			System.out.println("predict error 0");
		}
		else {
			agent.Update(rewardWme, 0);
			System.out.println("predict error 1");
		}
		agent.Commit();
	}

	/**
     * a handy debugging method for printing the current state and command
     * in human readable format.
     */
    private void printState(int cmd)
    {
        switch(this.currentState)
        {
            case STATE_1:  
                System.out.print("In State ONE ");
                break;
            case STATE_2:
                System.out.print("In State TWO ");
                break;
            default:
                System.out.print("In State UNKNOWN!! ");
                break;
        }//switch                    
            
        switch(currentDirection)
        {
            case DIR_LEFT:
                System.out.print(" moving LEFT");
                break;
            case DIR_RIGHT:
                System.out.print(" moving RIGHT");
                break;
            case DIR_UP:
                System.out.print(" moving UP");
                break;
            default:
                System.out.print(" moving ????");
                break;
        }//switch

        System.out.println(" expecting reward of " + cmd);
        
    }//printState

	@Override
	public void printEventHandler(int arg0, Object arg1, Agent arg2,
			String arg3) {
		System.out.println(arg3.trim());

	}

	@Override
	public void run() {
		cli();
		
		kernel.DestroyAgent(agent);
		kernel.Shutdown();

		// need to explicitly call exit() if we want to end cleanly
		// see note in TowersOfHanoi.java:main
		System.exit(0); 
	}
}
