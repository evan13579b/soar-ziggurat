import sys
import re
from warnings import warn


if __name__ == "__main__":
	fn = sys.argv[1]
	f = open(fn, 'r')

	#
	# keep a map of
	#  rulename -> (pattern, [ (dc, n, q, v) .... ] )
	
	lines = f.readlines()
	f.close()
	print "read %d lines" % len(lines)
	rules_re = re.compile("Sorted rules @ decision  (\d+)")
	rulemap = {}
	buffer = {}
	i = 0
	level_re = re.compile("\^level-(\d+) (..)")
	dir_re = re.compile("\^dir (.)")

	threshold = 6
	
	while i < len(lines):

		l = lines[i]
		m = rules_re.search(l)
		if m:
			#print "Match at decision:", m.group(1)
			dc = int(m.group(1))

			i += 2
			if not lines[i].startswith('---'): warn("Expected a hrule!")
			i += 1
			while not lines[i].startswith('---'):
				lc = lines[i].split()
				rulename = lc[0].strip()
				n = float(lc[1].strip())
				q = float(lc[2].strip())
				v = float(lc[4].strip())
				
				if rulemap.has_key(rulename):
					list = rulemap[rulename][1]
					if list[-1] != (dc, n, q, v):
						list.append( (dc, n, q, v) )
				else:
					if buffer.has_key(rulename): warn("didn't expect this!")
					buffer[rulename] = (dc, n, q, v)
				i += 1
			
			if i+1 < len(lines) and lines[i+1].startswith('New rules'):
				i += 2
				while not lines[i].startswith('---'):
					if lines[i].startswith('sp {'):
						rulename = lines[i][4:].strip()
						conditions = []
						dir = None
						i += 1
						while lines[i].strip():
							#print "searching", lines[i]
							for m in level_re.finditer(lines[i]):
								conditions.append( (int(m.group(1)), str(m.group(2))) )
							m = dir_re.search(lines[i])
							if m:
								dir = m.group(1)
							i += 1
						# kick out of rule....
						#print rulename, conditions, dir
						# this rule should be in the buffer, but not in the map
						if not buffer.has_key(rulename): warn("??")
						if rulemap.has_key(rulename): warn("!!")
						conditions.sort(reverse=True)
						cond_str = '-'.join([c[1] for c in conditions])
						if "yes" in rulename: p = "YES"
						else: p = "NO"
						rulemap[rulename] = ("%s-?%s->%s"%(cond_str,dir,p), [buffer[rulename]])
						del buffer[rulename]
 					else:
						i += 1
						
		i += 1
		for k in buffer:
			if rulemap.has_key(k): warn("unexpected!")
			rulemap

	print "Buffer:"
	print buffer
	print "RM"
	key_order = [(len(rulemap[k][0]),k) for k in rulemap]
	key_order.sort()
	
	for k in key_order:
		r = k[1]
		print r
		print "  ", rulemap[r][0]
		print "  ", rulemap[r][1][-1]


	print "\n\n\n"
	print "OVER THRESHOLD", threshold
	print "\n\n\n"
	n = 0
	for k in key_order:
		r = k[1]
		if rulemap[r][1][-1][1] > threshold:
			print r
			print "  ", rulemap[r][0]
			print "  ", rulemap[r][1][-1]
			n += 1
	print n, 'rules were over threshold', threshold
