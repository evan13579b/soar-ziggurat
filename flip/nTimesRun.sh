#!/bin/bash

basename=$1
cycles=$2
n=$3


fileList=()
for i in `seq 1 $n`
do
tempMark="-$i"
tempname=$basename$tempMark".csv"
./all-run.sh $tempname $cycles
fileList+=($tempname)
done

extension=".csv"

python csvAverage.py $basename$extension ${fileList[@]}
