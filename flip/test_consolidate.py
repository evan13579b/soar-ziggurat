import unittest
import consolidate as c
import numpy as np

class ConsolidateTest(unittest.TestCase):

	def setUp(self):
		self.R1_p = np.array([1,1,0,1,0,0,0,0,1])
		self.R1_o = np.array([1,1,0,0,0,0,0,0,0])
		self.R1_r = np.array([2,4,6,6,6,6,6,6,7])
		
		self.R2_p = np.array([1,0,0,0,1,0,1,0,0])
		self.R2_o = np.array([0,1,0,0,0,0,0,0,0])
		self.R2_r = np.array([2,4,4,4,4,4,5,5,5])

		self.R3_p = np.array([1,0,0,0,1,1,1,0,1])
		self.R3_o = np.array([1,0,1,0,0,1,0,0,0])
		self.R3_r = np.array([2,4,6,6,6,6,6,6,7])

	def test_load1(self):
		
		(decisions, predictions, outcomes, rules, dirs) = c.load_data(['tests/r10_1d.csv'])
		# check decisions
		self.assertTrue( (decisions == np.arange(9)).all() )
		# check predictions
		self.assertTrue( (predictions == self.R1_p).all() )
		# check outcomes
		self.assertTrue( (outcomes == self.R1_o).all() )
		# check rules
		self.assertTrue( (rules == self.R1_r).all() )


	def test_load2(self):
		(decisions, predictions, outcomes, rules, dirs) = c.load_data(['tests/r10_2d.csv'])
		# check decisions
		self.assertTrue( (decisions == np.arange(9)).all() )
		# check predictions
		self.assertTrue( (predictions == self.R2_p).all() )
		# check outcomes
		self.assertTrue( (outcomes == self.R2_o).all() )
		# check rules
		self.assertTrue( (rules == self.R2_r).all() )

	def test_load3(self):
		(decisions, predictions, outcomes, rules, dirs) = c.load_data(['tests/r10_3d.csv'])
		# check decisions
		self.assertTrue( (decisions == np.arange(9)).all() )
		# check predictions
		self.assertTrue( (predictions == self.R3_p).all() )
		# check outcomes
		self.assertTrue( (outcomes == self.R3_o).all() )
		# check rules
		self.assertTrue( (rules == self.R3_r).all() )


	def test_load1_and_2_and_3(self):
		(decisions, predictions, outcomes, rules, dirs) = c.load_data(['tests/r10_1d.csv',
																 'tests/r10_2d.csv',
			                                                     'tests/r10_3d.csv'])
		# check decisions
		self.assertTrue( (decisions == np.arange(9)).all() )

		# check predictions
		self.assertTrue( (predictions == np.column_stack([self.R1_p,
														  self.R2_p,
			                                              self.R3_p])).all() )

		# check outcomes
		self.assertTrue( (outcomes == np.column_stack([self.R1_o, self.R2_o,
													   self.R3_o])).all() )

		# check rules

		print (self.R1_r+self.R2_r+self.R3_r)/3.0
		self.assertTrue( (rules == (self.R1_r+self.R2_r+self.R3_r)/3.0).all() )

		(r,y,n) = c.correct_predictions(predictions, outcomes)
		cr = np.column_stack( [ [1,1,1,0,1,1,1,1,0],
								[0,0,1,1,0,1,0,1,1],
								[1,1,0,1,0,1,0,1,0] ] ) == 1
		cy = np.column_stack( [ [1,1,0,0,0,0,0,0,0],
								[0,0,0,0,0,0,0,0,0],
								[1,0,0,0,0,1,0,0,0] ]) == 1
		cn = np.column_stack( [ [0,0,1,0,1,1,1,1,0],
								[0,0,1,1,0,1,0,1,1],
								[0,1,0,1,0,0,0,1,0] ]) == 1
		
		self.assertTrue( (r == cr).all() )
		self.assertTrue( (r == np.column_stack([self.R1_p==self.R1_o,
												self.R2_p==self.R2_o,
			                                    self.R3_p==self.R3_o])).all() )

		self.assertTrue( (y == cy).all() )
		self.assertTrue( (n == cn).all() )
		self.assertTrue( (cr.sum(axis=1) == np.array([2,2,2,2,1,3,1,3,1])).all() )
		
if __name__ == '__main__':
    unittest.main()

						 
