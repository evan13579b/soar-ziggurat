#!/bin/bash

DIR=$1
BIN=`pwd`/resources/plotting

cd ${DIR}
for f in `ls results*`; 
  do
  echo "numberifying $f" 
  ${BIN}/numberify-results.sh $f
done

cd -

echo "consolidating data"
python ${BIN}/consolidate.py --consolidate --cname ${DIR}/consolidated.npz ${DIR}/numeric-results*
echo "plotting"
python ${BIN}/consolidate.py --plot ${DIR}/consolidated.npz

mv plot*pdf ${DIR}