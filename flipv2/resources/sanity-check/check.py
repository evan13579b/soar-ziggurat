
import unittest
import csv
import os

TESTPATH = 'test-data'
class Test(unittest.TestCase):

    def testFiles(self):
        results = open(os.path.join(TESTPATH, 'results-test.csv'))
        resultscsv = csv.DictReader(results)
        results_rows = [row for row in resultscsv]
        results.close()

        history = open(os.path.join(TESTPATH,'history-test.dat'))
        history_rows = [row.strip() for row in history.readlines()]
        history.close()

        # history will not include a 'final' episode
        self.assertEqual(len(history_rows), len(results_rows)-1)

        # check that the episodes map to the history
        for (i,r) in enumerate(history_rows):
            dir = r[0]
            outcome = r[1]
            self.assertEqual(dir, results_rows[i]['dir'])
            self.assertEqual(outcome, results_rows[i]['outcome'])
            
        # ensure there are no U1 episodes
        for r in history_rows:
            self.assertNotEqual(r, "U1")

        # ensure that the state goes from A to B on a Left action and vice versa
        # ensure that the outcome is only 1 when the state changes
        for r in results_rows:
            if r['state'] == 'State-A' and r['dir'] == 'R':
                self.assertTrue( r['outcome'] == '1' )
                self.assertTrue( r['next-state'] == 'State-B' )

            elif r['state'] == 'State-B' and r['dir'] == 'L':
                self.assertTrue( r['outcome'] == '1' )
                self.assertTrue( r['next-state'] == 'State-A' )
            else:
                self.assertTrue( r['outcome'] == '0' )
        # ensure some learning happened...
        # compare success rate in first 50 events to last 50 events
        # this should *GENERALLY* hold, but not strictly...
        first_success = 0
        for r in results_rows[:50]:
            if r['outcome'] == r['prediction']:
                first_success += 1
        last_success = 0
        for r in results_rows[-50:]:
            if r['outcome'] == r['prediction']:
                last_success += 1
        print "First success:", first_success
        print "Last success: ", last_success
        self.assertTrue( last_success > first_success )
        
if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        TESTPATH = sys.argv.pop(1)
    unittest.main()
