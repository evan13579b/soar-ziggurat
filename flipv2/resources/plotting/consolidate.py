import argparse
import sys
import numpy
from pylab import *
from matplotlib import rc
from warnings import warn
from smooth import smooth

rc('text', usetex=True)
rc('font', family='serif')
rc('font', serif='Times')

def consolidate(files,saveto='consolidated.npz'):
	"""Loads the data from the .csv files into numpy arrays and saves for later use"""

	decisions = None
	last_header = None
	for f in files:
		fo = open(f, 'r')
		header = fo.readline()
		if last_header != None:
			if header != last_header:
				warn("headers don't match!")
				sys.exit(1)
		last_header = header
			
		a = numpy.loadtxt(fo, delimiter=',')
		
		if decisions == None:
			hl = [h.strip() for h in header.split(',')]
			if hl[0] != 'decision-cycle': warn("Expected column 'decision-cycle'")
			decisions = a[:,0]
			if hl[3] != 'prediction': warn("Expected column 'prediction'")
			predictions = a[:,3]
			if hl[4] != 'outcome': warn("Expected column 'outcome'")
			outcomes = a[:,4]
			if hl[8] != 'total-rulecount': warn("Expected column 'total-rulecount'")
			rules = a[:,8]
			if hl[2] != 'dir': warn("Expected column 'dir'")
			directions = a[:,2]
			if hl[9] != 'template-count': warn("Expected column 'template-count'")
			templates = a[:,9]
			if hl[10] != 'template-depth': warn("Expected column 'template-depth'")
			depth = a[:,10]
		else:
			if len(a) > len(decisions):
				predictions = numpy.column_stack([predictions, a[:len(decisions),3]])
				outcomes = numpy.column_stack([outcomes, a[:len(decisions),4]])
				rules = rules + a[:len(decisions),8]
				directions = numpy.column_stack([directions, a[:len(decisions),2]])
				templates = templates + a[:len(decisions),9]
				depth = depth + a[:len(decisions),10]
			else:
				decisions = decisions[:len(a)]
				predictions = numpy.column_stack([predictions[:len(decisions)], a[:,3]])
				outcomes = numpy.column_stack([outcomes[:len(decisions)], a[:,4]])
				rules = rules[:len(decisions)] + a[:,8]
				directions = numpy.column_stack([directions[:len(decisions)], a[:,2]])
				templates = templates[:len(decisions)] + a[:,9]
				depth = depth[:len(decisions)] + a[:,10]
				
	rules = rules / float(len(files))
	templates = templates / float(len(files))
	depth = depth / float(len(files))

	consolidated = {'decisions':decisions,
					'predictions':predictions,
					'outcomes':outcomes,
					'rules':rules,
					'directions':directions,
					'templates':templates,
					'depth':depth}
	numpy.savez(saveto, **consolidated)
	return consolidated

def smooth2(x, window_len, mode='flat'):

	smoothed = []
	for i in range(len(x)):
		start = max(i-window_len, 0)
		end = i
		smoothed.append( x[start:end+1].sum() / float(end-start+1) )
	return numpy.array(smoothed)


def generate_one_agent_learning_curve(agent_data):
	if type(agent_data) == str:
		agent_data = numpy.load(agent_data)

	(cp, cpy, cpn) = get_correct_predictions(agent_data)
	cp_width = float(cp.shape[1])

	# at each decision (row), sum up the columns that are correct
	print cp[0]
	print cp[0].sum()
	
	p_error = 1.0 - (cp.sum(axis=1)/cp_width)
	print len(agent_data['decisions'])
	print "Error @5K (+-5)", p_error[5000-5:5000+5] 
	print "Error @10K (+-5)", p_error[10000-5:10000+5]
	print "Mean:", p_error[10000-5:10000+5].mean()
	print "Error Between 10K-11K", p_error[10000:11000].mean()
	print "Error Between 10K-20K", p_error[10000:20000].mean()
	p_error_sm = smooth2(p_error, 100, 'flat')

	plot( agent_data['decisions'], p_error_sm, 'b-' )
	ylim([0,1])
	xlim([0,100])
	title("Prediction Error")
	savefig('plot-p_error_1a_100.pdf')
	xlim([0,10000])
	title("Prediction Error")
	savefig('plot-p_error_1a_10000.pdf')
	xlim(0,len(agent_data['decisions']))
	savefig('plot-p_error_1a_alldecisions.pdf')
	clf()
	
def generate_one_agent_yn_learning_curve(agent_data):
	if type(agent_data) == str:
		agent_data = numpy.load(agent_data)

	(cp, cpy, cpn) = get_correct_predictions(agent_data)
	cpy_width = float(cpy.shape[1])
	cpn_width = float(cpn.shape[1])
	no = (agent_data['predictions'] == 0)
	yes = (agent_data['predictions'] == 1)

	# at each decision (row), sum up the columns that are correct
	print cp[0]
	print cp[0].sum()
	
	py_error = 1.0 - (cpy.sum(axis=1)/yes.sum(axis=1,dtype=numpy.float64))
	pn_error = 1.0 - (cpn.sum(axis=1)/no.sum(axis=1,dtype=numpy.float64))
	print len(agent_data['decisions'])
	print "Error @5K (+-5)", py_error[5000-5:5000+5] 
	print "Error @10K (+-5)", py_error[10000-5:10000+5]
	print "Mean:", py_error[10000-5:10000+5].mean()
	print "Error Between 10K-11K", py_error[10000:11000].mean()
	print "Error Between 10K-20K", py_error[10000:20000].mean()
	py_error_sm = smooth2(py_error, 200, 'flat')
	pn_error_sm = smooth2(pn_error, 200, 'flat')

	(l1, l2) = plot( agent_data['decisions'], py_error_sm, 'b-',
					 pn_error_sm, 'g-')
	ylim([0,1])
	xlim([0,10000])
	title("Prediction Error")
	legend( (l1, l2), ('"Yes" Predictions', '"No" Predictions'), 'upper left')
	savefig('plot-p_error_1a_yn.pdf')
	clf()

def generate_rule_growth_curve(agent_data):
	if type(agent_data) == str:
		agent_data = numpy.load(agent_data)

	cla()
	(l1,l2) = plot(agent_data['decisions'], agent_data['rules'], 'b-',
		 agent_data['templates'], 'g-')
	xlim([0,10000])


	ax = gca()
	#ax.set_aspect(2)
	for tick in ax.xaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
	for tick in ax.yaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
		
	xlabel("Decisions",fontsize=20)
	ylabel("Number of Rules",fontsize=20)
	legend( (l1, l2), ('Total Rules', 'Templates'), 'upper left')
	savefig('plot-rule_growth.pdf')
	clf()


def generate_multi_agent_learning_curve(dir_or_dict_and_name_list):
	all_agent_data = {}
	for d,n in dir_or_dict_and_name_list:
		if type(d) == str:
			# then d is a dir so load the data.
			all_agent_data[n] = numpy.load(d)
		else:
			# then d is an array...
			all_agent_data[n] = d

	data_dict = {}
	for agent_name in all_agent_data:
		agent_data = all_agent_data[agent_name]

		
		(cp, cpy, cpn) = get_correct_predictions(agent_data)
		cp_width = float(cp.shape[1])

		# at each decision (row), sum up the columns that are correct
		print cp[0]
		print cp[0].sum()

		p_error = 1.0 - (cp.sum(axis=1)/cp_width)
		print '-'*80
		print "Agent name:", agent_name
		print " Initial Error:", p_error[0]
		print " Decisions in Trial:", len(agent_data['decisions'])
		print " Error @5K (+-5)", p_error[5000-5:5000+5] 
		print " Error @10K (+-5)", p_error[10000-5:10000+5]
		print " Mean @10K (+-5)", p_error[10000-5:10000+5].mean()
		print " Mean Error Between 10K-11K", p_error[10000:11000].mean()
		print " Mean Error Between 10K-20K", p_error[10000:20000].mean()
		p_error_sm = smooth2(p_error, 100, 'flat')
		data_dict[(agent_name,'er')] = p_error_sm

		
		#no = (predictions == 0)
		#yes = (predictions == 1)
		up = (agent_data['directions'] == 2)
		left = (agent_data['directions'] == 0)

		correct_up = np.ma.array(cp, mask=(agent_data['directions'] != 2))
		correct_left = np.ma.array(cp, mask=(agent_data['directions'] != 0))
		cs = cp.sum(axis=1)
		cy = cpy.sum(axis=1)
		cn = cpn.sum(axis=1)		
		assert (cs == (cy+cn)).all(), "correct != correct_yes + correct_no"
		
		cyu_error = (up.sum(axis=1) - correct_up.sum(axis=1))/up.sum(axis=1,dtype=np.float64)
		cyl_error = (left.sum(axis=1) - correct_left.sum(axis=1))/left.sum(axis=1,dtype=np.float64)

		cyu_error_sm = smooth(cyu_error, 111, 'flat')
		cyl_error_sm = smooth(cyl_error, 111, 'flat')
		
		data_dict[(agent_name,'yuer')] = cyu_error_sm
		data_dict[(agent_name,'yler')] = cyl_error_sm


	longest_run = []
	for n in all_agent_data:
		if len(all_agent_data[n]['decisions']) > len(longest_run):
			longest_run = all_agent_data[n]['decisions']

	names = [n for n in all_agent_data]
	make_plots(names, longest_run, data_dict, 'er')
	make_plots(names, longest_run, data_dict, 'yuer')
	make_plots(names, longest_run, data_dict, 'yler')
	
def make_plots(names, longest_run, data_dict, field):
	field_name = {'er':'Prediction Error',
				  'yuer':'Up Action Yes-Prediction Error',
				  'yler':'Left Action Yes-Prediction Error'}
	
	assert field in field_name

	clf()
	colors = ['r-', 'g-', 'b-']
	lines = []
	for n,name in enumerate(names):
		lines.extend(plot( longest_run, data_dict[(name, field)], colors[n] ))


	ylim([0,.5])
	ax = gca()
	for tick in ax.xaxis.get_major_ticks():
		tick.label1.set_fontsize(18)
	for tick in ax.yaxis.get_major_ticks():
		tick.label1.set_fontsize(18)

	xlabel("Decisions",fontsize=20)
	ylabel(field_name[field],fontsize=20)
	legend( lines, [n for n in names])

	# find lengths of interest:
	xlimits = [100, 1000, len(longest_run)/2.0, len(longest_run)]
	if len(longest_run) > 10000:
		xlimits.append(10000)
		xlimits.append(5000)
	for xl in xlimits:
		xlim([0,xl])
		savefig('plot-%s_ma-%d.pdf' % (field,xl))

	
def get_correct_predictions(data):
	correct_predictions = (data['predictions'] == data['outcomes'])
	# find the predictions that were 'yes'
	#	correct_predict_yes = np.ma.array((predictions==1),
	#                                     mask=np.logical_not(correct_predictions))

	correct_predict_no = np.ma.array(correct_predictions,
									 mask=(data['predictions'] == 1))

	correct_predict_yes = np.ma.array(correct_predictions,
									  mask=(data['predictions'] == 0))
	
	return (correct_predictions, correct_predict_yes, correct_predict_no)

if __name__ == "__main__":
	import os
	
	parser = argparse.ArgumentParser()
	parser.add_argument('files', nargs='*',
						help="a list of the files to consolidate")
	parser.add_argument('--cname', default="consolidated.npz",
						help="the name of the .npz file to store")
	parser.add_argument('--plot', action='store_true',
						help="generate plot(s) from one or more .npz files")

	parser.add_argument('--consolidate', action='store_true',
						help="generate the multi agent plots...")

	args = parser.parse_args()

	if args.consolidate:
		arrayz = consolidate(args.files, args.cname)

	elif args.plot:
		if len(args.files) == 1:
			# just one
			generate_one_agent_yn_learning_curve(args.files[0])
			generate_one_agent_learning_curve(args.files[0])
			generate_rule_growth_curve(args.files[0])
		else:	
			pn = [(f,os.path.dirname(f)) for f in args.files]
			print pn
			generate_multi_agent_learning_curve(pn)
	else:
		print "Hmmm."
		sys.exit(1)
		
		

