#!/usr/bin/env python2.7


#
# flip_predict.py
# An implementation of the Ziggurat.FlipPredictEnvironment
#
#  Based on the example python soar environment
#  http://code.google.com/p/soar/wiki/ExampleCode_SoarExp
#

from os import environ as env, fsync
from subprocess import call, check_output, CalledProcessError, STDOUT
import re
import sys
import random
import time
import os
from warnings import warn


if "DYLD_LIBRARY_PATH" in env:
	LIB_PATH = env["DYLD_LIBRARY_PATH"]
elif "LD_LIBRARY_PATH" in env:
	LIB_PATH = env["LD_LIBRARY_PATH"]
else:
	print "Soar LIBRARY_PATH environment variable not set; quitting" 
	exit(1)
sys.path.append(LIB_PATH)
import Python_sml_ClientInterface as sml

# low level Soar functions

def create_kernel():
	kernel = sml.Kernel.CreateKernelInCurrentThread()
	if not kernel or kernel.HadError():
		print "Error creating kernel: " + kernel.GetLastErrorDescription()
		exit(1)
	return kernel

def create_agent(kernel, name):
	agent = kernel.CreateAgent("flip")
	if not agent:
		print "Error creating agent: " + kernel.GetLastErrorDescription()
		exit(1)
	return agent

# mid-level framework


def cli(agent):
	cmd = raw_input("soar> ")
	cmd_parts = cmd.split()
	while cmd_parts[0] not in ("exit", "quit"):
		if cmd_parts[0] == "slave":
			slave(cmd, cmd_parts)
		elif cmd:
			print agent.ExecuteCommandLine(cmd).strip()
		cmd = raw_input("soar> ")
		cmd_parts = cmd.split()


def slave(cmd, cmd_parts):
	"""Enter Slave Mode, waiting for commands from an external connection...."""
	
	if cmd_parts[0] == "slave":
		# print a banner...
		log_file = sys.stdout
		decisions = None
		print "Entering slave mode:"
		if len(cmd_parts) > 1:
			log_filename = cmd_parts[1]
			print "  - log file '%s'...." % log_filename
			log_file = open(log_filename, 'w')
		if len(cmd_parts) > 2:
			decisions = int(cmd_parts[2])
			print "  - will exit slave mode after %d decisions" % decisions
		else:
			print "  - will remain in slave mode indefinitely...kill process to exit."
	
		print "  waiting for commands from an externally connected sml process..."


		
		i = 0
		spinner = ['-','/','|','\\']
		dc_count = agent.GetDecisionCycleCounter()
		if decisions != None:
			decisions = dc_count + decisions
		polling_pass = 0
		
		# now, enter a loop where we're polling for incoming commands.
		# instead of just busy waiting, we'll loop through as fast as possible
		# for a few iterations, and then start sleeping until we get commands
		# we'll track our passes through the loop waiting with polling_pass
		while True:

			# is it time to exit slave mode?
			if dc_count == decisions:
				print "Reached %d decisions. leaving slave mode" % decisions
				break
			
			has_commands = kernel.CheckForIncomingCommands()
			if not has_commands:
				# no commands yet... we might be waiting...
				# if we've been through the loop a few times, start sleeping
				if polling_pass > 2:
					print "sleeping..."
					if polling_pass < 5:
						time.sleep(.1)
					else:
						time.sleep(.7)
				polling_pass += 1
			else:
				# something happened, but what?
				dc_now = agent.GetDecisionCycleCounter()
				if dc_now != dc_count + 1:
					warn("Command didn't step one decision as expected...")
				polling_pass = 0
				dc_count = dc_now
	      

			
			if dc_count % 10 == 1:
				process_rl_cmd( agent.ExecuteCommandLine("print --rl"), log_file )

				
			sys.stdout.write(spinner[i])
			sys.stdout.flush()
			#time.sleep(.1)
			sys.stdout.write('\b')
			i += 1
			i %= len(spinner)


		
def process_rl_cmd(rlstring, stream=sys.stdout, _rule_memo={}):
	"""Process the string returned by the 'print --rl' command

	_rule_memo and _rule_name_memo should *NOT* be supplied by the caller,
	these are internally used memoization dictionaries
	"""
	
	rules = []
	for l in rlstring.split('\n'):
		elements = l.split()
		if len(elements) > 0:
			if len(elements) != 5:
				print "WARNING: unexpected number of elements!"
			else:
				(rule_name, n, q, mean, variance) = elements
				rules.append((rule_name, float(n), float(q), float(mean), float(variance)))

	s_rules = [(r[2], r[3], r) for r in rules]
	s_rules.sort()
	current_dc = agent.GetDecisionCycleCounter()
	print >>stream, "Sorted rules @ decision ", current_dc
	print >>stream, "%-60s %9s %9s %9s %9s" % ("rule name", "n", "q", "mean", "var")
	print >>stream, "-"*90
	has_new_rules = False
	for r in s_rules:

		print >>stream, "%-60s %9.5f %9.5f %9.5f %9.5f" % r[2]
		# keep track of the rules we've seen and when we've first seen them
		if not r[2][0] in _rule_memo:
			_rule_memo[r[2][0]] = current_dc
			has_new_rules = True

	print >>stream, "-"*90
	if has_new_rules:
		print >>stream, "New rules:"
		for r in _rule_memo:
			if _rule_memo[r] == current_dc:
				print "rule alias: '%s'" % r[2][0]
				print >>stream, agent.ExecuteCommandLine("print %s" % r)
				print ""
		print >>stream, "-"*90

# callback registry
def register_print_callback(kernel, agent, function, user_data=None):
	agent.RegisterForPrintEvent(sml.smlEVENT_PRINT, function, user_data)

def register_output_callback(kernel, agent, function, user_data=None):
	agent.RegisterForRunEvent(sml.smlEVENT_AFTER_OUTPUT_PHASE, function, user_data)

def register_output_change_callback(kernel, agent, function, user_data=None):
	kernel.RegisterForUpdateEvent(sml.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, function, user_data)

def register_destruction_callback(kernel, agent, function, user_data=None):
	agent.RegisterForRunEvent(sml.smlEVENT_AFTER_HALTED, function, user_data)

# callback functions

def callback_print_message(mid, user_data, agent, message):
	print message.strip()


STATE_A = "State-A"
STATE_B = "State-B"

DIR_WME = None
SEE_WME = None
REWARD_WME = None



def getNumberOfRules(agent):
	global AGENT_STATS
	
	rlRules=agent.ExecuteCommandLine("print --rl")
	AGENT_STATS["predict-yes-rulecount"] = rlRules.count("predict-yes")
	AGENT_STATS["predict-no-rulecount"] = rlRules.count("predict-no")
	AGENT_STATS["total-rulecount"] = AGENT_STATS["predict-no-rulecount"]+AGENT_STATS["predict-yes-rulecount"]

	templates=agent.ExecuteCommandLine("print --template")
	ntemplates = 0
	# a poor mans way of charting the template depth is to count the number of H's
	# in the template and subtract 1
	depth = 0
	for l in templates.split('\n'):
		if l.strip() != "": ntemplates += 1
		if l.count("H")-1 > depth:
			depth = l.count("H")-1
	AGENT_STATS["template-count"] = ntemplates
	AGENT_STATS["template-depth"] = depth


def pickDirectionRandomly():
	#return random.choice(_DIR_MAP.keys())
	
	dir=random.choice(['L','R','U'])
	print "dir: dir is"+str(dir)
	return dir

def callback_output(mid, user_data, agent, *args):
	global SEE_WME, AGENT_STATS, resultsFile
	

	assert agent.GetNumberCommands() <= 1

	AGENT_STATS['decision-cycle'] = agent.GetDecisionCycleCounter()
	AGENT_STATS['prediction'] = '?'
	
	for n in range(agent.GetNumberCommands()):
		cid = agent.GetCommand(n)

		assert cid.GetCommandName() in ['predict-yes', 'predict-no']

		if cid.GetCommandName() == "predict-yes": AGENT_STATS['prediction'] = 1
		if cid.GetCommandName() == "predict-no": AGENT_STATS['prediction'] = 0

		cid.AddStatusComplete()
		agent.Commit()
		agent.ClearOutputLinkChanges()
		print "ENV: Agent did:", cid.GetCommandName(), "for direction", AGENT_STATS['dir'] , "in state", AGENT_STATS['state'] 


	if AGENT_STATS['prediction'] == '?':
		raise "This is a problem ;)"

	print_state()

	if AGENT_STATS['state'] == STATE_A and AGENT_STATS['dir'] == 'R':
		AGENT_STATS['next-state'] = STATE_B
		AGENT_STATS['outcome'] = 1
	elif AGENT_STATS['state'] == STATE_B and AGENT_STATS['dir'] == 'L':
		AGENT_STATS['next-state'] = STATE_A
		AGENT_STATS['outcome'] = 1

	else:
		AGENT_STATS['next-state'] = AGENT_STATS['state']
		AGENT_STATS['outcome'] = 0
		
	print "ENV: (next state, see, prediction correct?) = (%s, %s, %s)" % (AGENT_STATS['next-state'], AGENT_STATS['outcome'], str(AGENT_STATS['prediction']==AGENT_STATS['outcome']))
	
	getNumberOfRules(agent)

	if resultsFile:
		print >>resultsFile, ",".join([str(AGENT_STATS[x]) for x in AGENT_STATS_HEADER])
		resultsFile.flush()

	agent.Update(SEE_WME, AGENT_STATS['outcome'])

	if AGENT_STATS['prediction'] == AGENT_STATS['outcome']:
		agent.Update(REWARD_WME, 1)
		print "predict error 0"
	else:
		agent.Update(REWARD_WME, 0)
		print "predict error 1"

	new_stats = {'state':AGENT_STATS['next-state']}
	AGENT_STATS = new_stats
	
	AGENT_STATS['dir'] = pickDirectionRandomly()
	agent.Update(DIR_WME, AGENT_STATS['dir'])


	agent.Commit()

def print_state():
	global AGENT_STATS

	print "In ", AGENT_STATS['state'], "moving", AGENT_STATS['dir']
	


AGENT_STATS_HEADER=["decision-cycle",
				"state","dir","prediction","outcome","next-state",
				"predict-yes-rulecount","predict-no-rulecount",
				"total-rulecount","template-count","template-depth"]		
		
AGENT_STATS = {}

if __name__ == "__main__":
	import argparse
	
	parser = argparse.ArgumentParser()
	parser.add_argument('--seed', type=int,
						help="seed python's random number generator")
	parser.add_argument('--results', help="results file")
	parser.add_argument('sourcefiles', nargs="*",
						 help="a list of .soar files (seperated by spaces) to load")
	
	args = parser.parse_args()
	
	if args.seed != None:
		print "Seeding...", args.seed
		random.seed(args.seed)
	if args.results:	
		resultsFile = open(args.results,"w")
		print >>resultsFile, ",".join(AGENT_STATS_HEADER)
		resultsFile.flush()
	else:
		resultsFile = None
		
	
	kernel = create_kernel()
	agent = create_agent(kernel, "flip")
	register_print_callback(kernel, agent, callback_print_message, None)
	register_output_callback(kernel, agent, callback_output, None)
	
	AGENT_STATS['state']= STATE_A
	AGENT_STATS['dir'] = pickDirectionRandomly()

	il = agent.GetInputLink()
	REWARD_WME = il.CreateIntWME( "reward", 0)
	DIR_WME = il.CreateStringWME( "dir", AGENT_STATS['dir'] )

	# The first value of see should never make it's way into an episode,
	# it's a sentinal, so we'll use -1 here to be clear about that.
	SEE_WME = il.CreateIntWME( "see", -1 )	

	print "Python-Soar Flip environment."
	print "To accept commands from an external sml process, you'll need to"
	print "type 'slave <log file> <n decisons>' at the prompt..."
	#  Python seems sensitive to threading as discussed in the Tcl entry
	#  in the sml documentation... 'Events in Tcl'
	#  http://code.google.com/p/soar/wiki/SMLQuickStartGuide
	if args.sourcefiles:
		for source in args.sourcefiles:
			print "sourcing " + source
			print agent.ExecuteCommandLine("source " + source)

		if args.seed != None:
			print "seeding Soar with %s ..." % (args.seed)
			print agent.ExecuteCommandLine("srand %s" % args.seed)

	else:
		print "sourcing 'flip_predict.soar'"
		if os.path.exists( "resources/soar" ):
			print agent.ExecuteCommandLine("source ./resources/soar/flip_predict.soar")
		elif os.path.exists( "../resources/soar" ):
			print agent.ExecuteCommandLine("source ../resources/soar/flip_predict.soar")
		if args.seed != None:
			print "seeding Soar with %s ..." % (args.seed)
			print agent.ExecuteCommandLine("srand %s" % args.seed)
			
	cli(agent)
	kernel.DestroyAgent(agent)
	kernel.Shutdown()
	del kernel
