

import sys

fname = sys.argv[1]

f = open(fname, 'r')
lines = [l.strip().split(',') for l in f.readlines()]
f.close()
print lines[0]
print len(lines)

for i in xrange(len(lines)-1, 0, -1):
	if lines[i][3] != lines[i][4]:
		print "Found Error at", lines[i][0]
		break
