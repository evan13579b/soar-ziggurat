#!/bin/bash

decisions=$1
seed_value=${2:-"X"}
date=`date "+%Y%m%d-%H%M%S"`
suffix=${3:-$date}
variant=${4:-"FPSL"}
estimator=${5:-"sl.TemplateTree"}

if [[ ${seed_value} == "X" ]]; then
  seed=""
else
  seed="--seed ${seed_value}"
fi

{ echo "slave rl-slave-${suffix}.log ${decisions}";
  echo "quit" 
 } | DYLD_LIBRARY_PATH=../Soar-Suite/out/ python python/flip_predict.py ${seed} --results results-${suffix}.csv > stdout-flip-${suffix}.txt &


envPID=$!
echo "environment pid is" $envPID

sleep 2

# Note -ea flag to turn on assertions
DYLD_LIBRARY_PATH=../Soar-Suite/out LD_LIBRARY_PATH=../Soar-Suite/out java -Xmx500M -ea -cp ../sl/bin/:../Soar-Suite/out/java/sml.jar:../Soar-suite/out/java/soar-smljava.jar:../LoopingSuffixTree/bin sl.${variant} ${decisions} history-${suffix}.dat ${estimator} > stdout-fpsl-${suffix}.txt 

ziggPID=$!
echo "ziggPID is " $ziggPID

if [ -e /usr/local/bin/dot ]; 
  then
    for i in `ls *.dot`; 
      do
        echo "Processing dot file: " $i
		dot -T pdf $i > ${i%.dot}.pdf
		dot -T svg $i > ${i%.dot}.svg
     done
fi
