package Ziggurat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import sml.Agent;
import sml.Identifier;
import sml.IntElement;
import sml.Kernel;
import sml.StringElement;
import sml.WMElement;
import sml.smlUpdateEventId;

/**
 * SoarListenerEnvironment
 * 
 * This is a base class upon which more specialized ziggurat environments
 * can be built to interact with soar... it may make sense in the future
 * to allow this environment to be used directly (see notes below).
 * 
 * Assumptions for running any SoarListenerEnvironment
 *  1) a Soar environment is running on the same machine as this Listener
 *  2) there is only one Soar environment running
 *  3) the environment has only 1 agent
 *  
 * How to use:
 *  - Launch the environment
 *  - Launch this Listener/or a subclass via the MCP
 * 
 *
 * Interaction with Soar is a bit at-odds with the standard Ziggurat model.
 * 
 * Normally, Ziggurat flow would looks like this:
 *   - start the MCP
 *   - MCP creates environment
 *   - MCP asks environment to create Agent
 *   - MCP gets state 0 from environment
 *   - MCP tells agent state 0 and gets action
 *   - MCP gets state 1 from environment
 *   etc.
 *   
 *   Here, however, the 'external' Soar connection contains elements of
 *   both the agent AND the environment, so the distinction for us is not
 *   particularly clean -- at least with the callbacks that I currently use.
 *   As a result, the model here looks somewhat different:
 *   
 *    SoarFlipPredictEventListener:
 *      responsibilities:
 *        - maintain links to Soar Kernel, and Agent
 *        - read Agent input/output and reward links
 *        - make state/action/reward data available for other consumers (e.g., ZigguSoar agent)
 *        
 *    ZigguSoar:
 *      responsibilities:
 *        - perform episodic learning ala Ziggurat
 *        
 *    ZigguSoar differs from Ziggurat in that the 'command' it selects
 *    is based on what Soar did, not on what Ziggurat's learning suggests
 *    
 *    
 * @author wallaces
 *
 */
public abstract class SoarListenerEnvironment extends Environment implements Kernel.UpdateEventInterface {

	private static Pattern SOAR_WME_PATTERN = Pattern.compile("\\((\\d+): (\\w+) \\^(\\S+) (\\S+)\\)");
	
	private static final int timesUsedThreshold=0;
	private static final float varianceThreshold=0.10f;

	private int cycleCounter=0;
	private int cycleLimit=10000;
	
	// variables prefixed with mt_ *could be* accessed on multiple threads
	// to be safe, they should be accessed in a synchronized block.
	// Currently, however, since the soar run command is issued from 
	// the client side, the callback will also be issued on this thread,
	// so they don't appear necessary... in the future, however, 
	// this might change.  The current implementation has a major downside:
	// you can't e.g., issue step commands from the debugger since it's all
	// controlled by this environment. Since we'll probably want to change
	// the implementation eventually, the mt_ notation will at least call 
	// attention to the fact that threading should be on the coder's radar.
	// see: http://code.google.com/p/soar/wiki/ThreadsInSML
	private String mt_stateAsString;
	private String mt_actionAsString;
	private String mt_lastRewardAsString;
	private int mt_nUpdates;
	
	private int mt_commandID;
	private WMESet mt_sensors;

	private ZigguSoar __zigg = null;
	private IntElement __zigg_state_id = null;
	
	// Thread local variables:
	// nu_  not the update callback
	Kernel nu_kernel;
	Agent nu_agent;
	int nu_commandID;
	WMESet nu_sensors;
	boolean nu_allowUnseenActions;
	
	ArrayList<StringElement> levelHashWMEs=new ArrayList<StringElement>();
	
	// this maps actions (a set of command features) to unique ids (integers)
	HashMap<Set<String>, Integer> nu_actionIDMap;
	HashMap<Integer, Set<String>> nu_idActionMap;

	// Simple episode class for easy storage in an array
	private class FlipEpisode{
		public String dir;
		public String see;
		
		public FlipEpisode(String see,String dir){
			this.see=see;
			this.dir=dir;
		}
		
		public String toString(){
			return see+dir;
		}
	}
	
	// Store a list of episodes for now for our simple version 2.
	private ArrayList<FlipEpisode> episodes=new ArrayList<FlipEpisode>();
	// And store a list of hashCodes
	private String[] hashCodes;
	// The level for our learner, initially at zero
	private int level=0;
	
	// set of names of rules we've already specialized
	private HashSet<String> rulesAlreadySpecialized=new HashSet<String>();
	
	/*
	 * Create the Listener Environment and attach to a running soar process.
	 *  
	 */
	public SoarListenerEnvironment() {		
		nu_kernel = Kernel.CreateRemoteConnection();
		if (nu_kernel.HadError()) {
			System.err.println("Couldn't connect to a remote kernel, is the environment running?");
			System.err.println(nu_kernel.GetLastErrorDescription());
			System.exit(0);
		}
		
		System.out.println("Connected to environment!");
		nu_kernel.RegisterForUpdateEvent(smlUpdateEventId.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, this, null) ;
		nu_agent = nu_kernel.GetAgentByIndex(0);
		System.out.println("Listening to the stream from agent: '" + nu_agent.GetAgentName() + "'...");
		mt_stateAsString = null;
		mt_actionAsString = null;
		mt_lastRewardAsString = null;
		mt_nUpdates = 0;
		
		
		this.levelHashWMEs.add(null); //adding this to make indexing of levelHashes more natural
		
		nu_actionIDMap = new HashMap<Set<String>,Integer>(500);
		nu_idActionMap = new HashMap<Integer, Set<String>>(500);

		// this will make the environment complain by default ;)
		nu_allowUnseenActions = false;
	}

	private String[] splitIntoLines(String text){
		return text.split("\n");
	}
	
	private String[] splitBySpaces(String text){
		return text.trim().split(" ");
	}
	
	private Hashtable<String,String> parsePrintRLLine(String printRLLineString){
		Hashtable<String,String> resultsMap=new Hashtable<String,String>();
		String[] parts=splitBySpaces(printRLLineString);
		if(parts.length!=6){
			System.out.println("print --rl did not have the right number of columns ("+parts.length+")");
		}
		else{
			resultsMap.put("Name", parts[0]);
			resultsMap.put("TimesUsed", parts[2]);
			resultsMap.put("Q-Value", parts[3]);
			resultsMap.put("RewardMean", parts[4]);
			resultsMap.put("RewardVariance",parts[5]);
		}
		return resultsMap;
	}
	
	private ArrayList<Hashtable<String,String>> parsePrintRL(String printRLString){
		boolean verbose=false;
		
		ArrayList<Hashtable<String,String>> resultsArray=new ArrayList<Hashtable<String,String>>();
		String[] lines=splitIntoLines(printRLString);
		for(String line:lines){
			Hashtable<String,String> parsedPrintRLLine=parsePrintRLLine(line);
			resultsArray.add(parsedPrintRLLine);
			
			if(verbose){
				System.out.println("line is:"+line);
				System.out.println("Name:"+parsedPrintRLLine.get("Name"));
				System.out.println("TimesUsed:"+parsedPrintRLLine.get("TimesUsed"));
				System.out.println("Q-Value:"+parsedPrintRLLine.get("Q-Value"));
				System.out.println("RewardMean:"+parsedPrintRLLine.get("RewardMean"));
				System.out.println("RewardVariance:"+parsedPrintRLLine.get("RewardVariance"));
			}
		}
		return resultsArray;
	}
	
	private ArrayList<Hashtable<String,String>> getProductionsToSpecialize(ArrayList<Hashtable<String,String>> parsedLines){
		
		
		ArrayList<Hashtable<String,String>> needsSpecialization=new ArrayList<Hashtable<String,String>>();
		float timesUsed;
		float rewardVariance;
		
		for(Hashtable<String,String> line:parsedLines){
			timesUsed=Float.parseFloat(line.get("TimesUsed"));
			if(line.get("Name").contains("rvt") && !this.rulesAlreadySpecialized.contains(line.get("Name")) && timesUsed>=SoarListenerEnvironment.timesUsedThreshold){
				rewardVariance=Float.parseFloat(line.get("RewardVariance"));
				if(rewardVariance>SoarListenerEnvironment.varianceThreshold){
					needsSpecialization.add(line);
				}
			}
		}
		return needsSpecialization;
	}
	
	// this returns a list of hashcodes up to the given level
	private String[] getNewHashCodes(ArrayList<FlipEpisode> episodes,int maxLevel){
		if(maxLevel>episodes.size()){
			System.out.println("ERROR: level too large for the number of episodes processed");
			return null;
		}
		
		int lastEpisodeIndex=episodes.size()-1;
		String[] newHashCodes=new String[maxLevel+1];
		String hashCode="";
		
		String episodeString="";
		newHashCodes[0]=episodeString;//.hashCode();
		for(int episodeNumber=lastEpisodeIndex, currentLevel=1;episodeNumber>=lastEpisodeIndex-maxLevel+1;episodeNumber--,currentLevel++){
			episodeString=episodes.get(episodeNumber).toString();
			hashCode=episodeString;//.hashCode();
			newHashCodes[currentLevel]=hashCode;
			//System.out.println("episodes->hash: "+episodeString+" -> "+hashCode);
		}
		return newHashCodes;
	}
	
	private int parseNameForLevel(String name){
		String[] parts=name.split("\\*");
		int lastIndex=parts.length-1;
		
		String levelPart=parts[lastIndex-1];
		String level=levelPart.substring(1);
		
		return Integer.parseInt(level);
	}
	
	private String createNewTemplate(String source,String name,int level){
		String[] lines=source.split("\n");
		int arrowIndex=0;
		for(int i=0;i<lines.length;i++){
			if(lines[i].trim().equals("-->")){
				arrowIndex=i;
			}
		}
		assert(arrowIndex!=0);
		
		// we have to get rid of the starting 'rl*' or else we eventually get a screen of nothing but rl*rl*rl*rl*rl so this line
		// has to be entirely recreated.
		String newName=name.substring(3)+"*H"+level;
		String newFirstLine="sp {"+newName+"\n";
		String templateLine=":template\n";
		for(int i=1;i<lines.length;i++){
			lines[i]+="\n";
		}
		String newSource="";
		newSource+=newFirstLine;
		newSource+=templateLine;
		for(int i=1;i<arrowIndex;i++){
			newSource+=lines[i];
		}
		String newHashLine="(<i2> ^level-"+level+" <level-"+level+">)\n";
		newSource+=newHashLine;
		for(int i=arrowIndex;i<lines.length;i++){
			newSource+=lines[i];
		}
		
		return newSource;
		
	}
	
	private int specializeProduction(Hashtable<String,String> production,Agent agent){
		String name=production.get("Name"); 
		this.rulesAlreadySpecialized.add(name); // add this to the set of already specialized rules so we don't keep specializing the same rules
		
		String source=agent.ExecuteCommandLine("print "+name);
		int currentLevel=parseNameForLevel(name);
		int nextLevel=currentLevel+1;
		
		String newSource=createNewTemplate(source,name,nextLevel);
		/*
		for(String line:source.split("\n")){
			System.out.println("oldSource:"+line);
		}
		for(String line:newSource.split("\n")){
			System.out.println("newSource:"+line);
		}
		*/
		//agent.ExecuteCommandLine("excise "+name);
		agent.ExecuteCommandLine(newSource);
		
		return nextLevel;
	}
	
	private int specializeProductions(ArrayList<Hashtable<String,String>> overgeneralList,Agent agent){
		int level,maxLevel=0;
		for(Hashtable<String,String> overgeneralProduction:overgeneralList){
			level=specializeProduction(overgeneralProduction,agent);
			if(level>maxLevel){
				maxLevel=level;
			}
		}
		return maxLevel;
	}
	
	private void sendHashCodesToSoar(Agent agent,String[] hashCodes){
		for(int i=1;i<hashCodes.length;i++){
			agent.Update(this.levelHashWMEs.get(i), hashCodes[i]);
		}
		agent.Commit();
	}
	
	/** This method is called when the "after_all_output_phases" event fires, at which point we update the world */
	public void updateEventHandler(int eventID, Object data, Kernel kernel, int runFlags)
	{
		try
		{
			this.cycleCounter++;
			// it's tempting to use the reference to the agent....
			// but get a thread local one
			Agent a = kernel.GetAgentByIndex(0);
			
			// grab the print --rl text, and parse each line getting the individual components
			// and then filter it by rules that have been ran at least 'SoarListenerEnvironment.timesUsedThreshold' times 
			// and have a variance greater than 'SoarListenerEnvironment.varianceThreshold'
			

			int newLevel=0;
			if(this.cycleCounter<=this.cycleLimit) {// && this.cycleCounter%10 == 0){
				String printRLString=a.ExecuteCommandLine("print --rl");
				
				ArrayList<Hashtable<String,String>> parsedLines=parsePrintRL(printRLString);
				ArrayList<Hashtable<String,String>> overgeneralList=getProductionsToSpecialize(parsedLines);
				newLevel=specializeProductions(overgeneralList,a);	
			}
			else{
				newLevel=level;
			}
				
			assert(newLevel==level || newLevel==level+1);
			
			if(newLevel>level){
				level=newLevel;
				levelHashWMEs.add(a.GetInputLink().CreateStringWME("level-"+level,""));
			}
			
			
			WMElement seeWME=a.GetOutputLink().FindByAttribute("see", 0);
			WMElement dirWME=a.GetOutputLink().FindByAttribute("dir", 0);
			if(seeWME!=null && dirWME!=null){
				String see=seeWME.GetValueAsString();
				String dir=dirWME.GetValueAsString();
				FlipEpisode episode=new FlipEpisode(see,dir);
				episodes.add(episode);
				hashCodes=getNewHashCodes(episodes,level);
				sendHashCodesToSoar(a,this.hashCodes);
			}
			else{
				System.out.println("Error, cannot see both 'see' and 'dir' and thus can't make this episode");		
			}
			
			
			

			// Evans change: I'm changing things so that information is read in java from the outputlink of soar. This necessitates that the flip simulation
			// is giving soar a reward based on the current prediction soar gives it but for the previous actual event from the environment's point of view.
			// Strangely I cannot decide on what to call the prediction soar gives in the python code. I call it the previous prediction but it really is
			// soars current prediction of what will happen after what the environment sees as the previous action.
			/*
			// get the state...
			String state = a.ExecuteCommandLine("print --depth 3 --internal i2").trim();
			System.out.println("reading tokens from input link data...");
			for(String z : state.split("[\r\n]+")) {
				System.out.println( "  - " + z);
			}

			// now do a similar thing for the action command....
			String action = a.ExecuteCommandLine("print --depth 3 --internal i3").trim();
			System.out.println("reading tokens from output link data...");
			for(String z : action.split("[\r\n]+")) {
				System.out.println( "  - " + z);
			}

			String lr = a.ExecuteCommandLine("print --depth 3 --internal r1").trim();
			System.out.println("reading tokens from reward link (for previous action)... ");
			for(String z : lr.split("[\r\n]+")) {
				System.out.println( "  - " + z);
			}*/

			synchronized(this) {
				//this is to satisfy some other part of the program that I don't think we are using and will eventually need to be rewritten.
				mt_stateAsString = "(26: I2 ^dir up)\n(27: I2 ^reward 0)\n(28: I2 ^zigg-state-id 0)";
				mt_actionAsString = "(36: I3 ^predict-yes N2)";
				mt_lastRewardAsString = "(29: R1 ^reward R5)";
				mt_nUpdates += 1;
			}
			//System.out.println("statey:"+state);
			//System.out.println("actioney:"+action);
			//System.out.println("rewardey:"+lr);
			
		} catch (Exception e) {
			System.err.println(e.toString());
			e.printStackTrace(System.err);
		}
	}

	protected abstract void printState(int cmd);
	
	public synchronized int getNUpdates() {
		return mt_nUpdates;
	}
	@Override
	/*
	 *  returns the previously acquired sensor data
	 *  
	 * @see Ziggurat.Environment#takeStep(int)
	 */
	public WMESet takeStep(int cmd)
    {
		 printState(cmd);
		 return stepSoar();
	}// takeStep	
	
	/**
	 * Ask Soar to Run until output.
	 * This will let us acquire new state/action/reward information
	 * 
	 * 'actions' here are tokenized so that Soar output link commands
	 *   are mapped to unique integers.  
	 * 
	 * @return a WMESet representing the current sensor information
	 */
	protected WMESet stepSoar() {
		nu_commandID = -1;
		nu_sensors = null;
		System.out.println("You asked Soar to take a step....");
		System.out.println("I believe my state is: " + __zigg.matchCode);
		if (__zigg_state_id == null) {
			// note: don't forget we need to filter these out of Zigg's state
			// in getWMESetFromInputLink...
			__zigg_state_id = nu_agent.CreateIntWME(nu_agent.GetInputLink(), "zigg-state-id",  __zigg.matchCode);
		} else {
			nu_agent.Update(__zigg_state_id, __zigg.matchCode);
		}
		// in theory synchronization isn't needed for this implementation
		// since we're issuing the RunSelf call, the updateEventHandler
		// should also be called on this thread.  That seems to occur
		// as the documentation would suggest.  IF, HOWEVER, the
		// environment was to issue the step/run call, the callback
		// would be called on a secondary thread (the SML Event thread)
		// and we'd need synchronization ... I've added it here 
		// so we don't forget this if the approach changes later.
		// see: http://code.google.com/p/soar/wiki/ThreadsInSML
		synchronized(this) {
			mt_actionAsString = null;
		}
		nu_agent.RunSelfTilOutput(); // this should block.
		while( true ) {
			synchronized(this) {
				if (mt_actionAsString != null) break;
			}
			try {
				System.out.println("waiting...");
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		synchronized(this) {
			// parse & tokenize the actionString
			nu_commandID = getCommandId(mt_actionAsString);
			nu_sensors = getWMESetFromInputLinkString(mt_stateAsString);
		}
		return nu_sensors;
	}

	
	private WMESet getWMESetFromInputLinkString(String stateAsString) {
		Hashtable<String, WME> state = new Hashtable<String, WME>(40);
		Matcher m = SOAR_WME_PATTERN.matcher(stateAsString);

		String attr, val;
		int lastMatch = 0;

		while( m.find(lastMatch) ) {
			//tt = m.group(1);
			//root = m.group(2);
			attr = m.group(3);
			val = m.group(4);
			lastMatch = m.end();

			// take all the attribute value pairs and put them in
			// the representation
			// for now, MOST EVERYTHING's a STRING
			if (attr.equals("zigg-state-id")) {
				int a=1; 
				//System.err.println("Skipping zig state there...."); // i can't figure out how to squelch this output at the command line
			} else {
				state.put(attr, new WME(attr, val, WME.Type.STRING)); 

			}

		}
		return new WMESet(state);

	}
	
	/**
	 * Look up a command and return it's integer id.
	 * @param cmdset
	 * @return
	 */
	private int getCommandId(String cmdAsString) {
		
		Set<String> cmdset = new HashSet<String>(10);
		Matcher m = SOAR_WME_PATTERN.matcher(cmdAsString);

		String attr, val;
		int lastMatch = 0;

		while( m.find(lastMatch) ) {
			attr = m.group(3);
			val = m.group(4);
			lastMatch = m.end();

			// BUG BUG we just take the attribute name
			// this works fine if the command has no parameters...
			cmdset.add(attr);
			
			if (attr.equals("complete"))
				System.err.println("WARNING: status complete command read.");
		}
		if (cmdset.size() == 0) return 0;

		System.out.println("___________ CMD SET BEGIN ___________");
		for (String q : cmdset) {
			System.out.println( "   '"+q+"'");
		}
		System.out.println("___________ CMD SET END ___________");
		Integer actionid = nu_actionIDMap.get(cmdset);
		if (actionid != null) {
			System.out.println(" ++ This is action ID " + actionid  + "  (" + nu_actionIDMap.size() + " known actions) ++");
		}
		else {
			System.out.println(" ++ This action is new! (" + nu_actionIDMap.size() + " actions previously seen) ++");
			if (this.nu_allowUnseenActions) {
				actionid = 100 + nu_actionIDMap.size(); // add 100 so it's easily distinguished from a state id
				nu_actionIDMap.put(cmdset, actionid);
				nu_idActionMap.put(actionid, cmdset);
			} else {
				System.err.println("Can't add an unseen action!");
				System.exit(1);
			}
			}
		return actionid;
	}

	
	@Override
	public WMESet generateCurrentWMESet() {
		nu_agent.ExecuteCommandLine("init-soar");
		return stepSoar();
		
	}

	@Override
	public abstract int getNumCommands();

	@Override
	public Ziggurat createAgentForEnvironment() {
		if (__zigg != null) return __zigg;
		
		ZigguSoar z = new ZigguSoar(this);
		__zigg = z;
		return __zigg;
	}
	

}
