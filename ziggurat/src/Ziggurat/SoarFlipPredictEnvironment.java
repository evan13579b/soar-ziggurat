package Ziggurat;

import java.util.HashSet;
import java.util.Set;

/**
 * A Ziggurat.Environment for the soar.FlipPredictEnvironment world
 * 
 * this class keeps it's own model of what is going on in
 * soar.FlipPredictEnvironment since we can't talk to it directly.
 * 
 * It also provides stringify methods to match the output from 
 * Ziggurat.FlipPredictEnvironment
 * 
 * 
 * @author wallaces
 *
 */
public class SoarFlipPredictEnvironment extends SoarListenerEnvironment {

	String nu_state;
	String nu_direction;

	public SoarFlipPredictEnvironment() {
		super();

		// we won't accept actions that we don't already know about.
		nu_allowUnseenActions = false;

		// To map to Ziggurat.FlipPredictEnvironment's implementation
		// we need to ensure that the 'predict-yes' action maps to
		// the action_id 1 and 'predict-no' maps to action id 0
		Set<String> ss = new HashSet<String>(5);
		ss.add("predict-yes");
		nu_actionIDMap.put(ss, 1);
		nu_idActionMap.put(1, ss);

		ss = new HashSet<String>(5);
		ss.add("predict-no");
		nu_actionIDMap.put(ss,  0);
		nu_idActionMap.put(0, ss);
		
	}
	/**
	 * Set up for the very first step of the environment...here
	 * set up our model.
	 */
	@Override
	public WMESet generateCurrentWMESet() {
		nu_state = "A";
		WMESet ret = super.generateCurrentWMESet();
		return markupWMESetWithZigguratReward(ret);
	}
	
	// we use a generic approach for getting WMEs from Soar
	// and putting them into a WMESet.
	// in particular, all the wmes currently come in from Soar 
	// as Strings.... this is ok, except that the MCP is looking
	// for (and counting) rewards that are doubles.... so we 
	// need to make sure those get there too.
	private WMESet markupWMESetWithZigguratReward(WMESet fromSoar) {
		WME r = fromSoar.sensors.get("reward");
		r.type = WME.Type.DOUBLE;
		return fromSoar;
	}

	/**
	 * When we step Soar, grab the direction from the sensor wmes
	 * so we can print it easily.
	 * @return
	 */
	@Override
	protected WMESet stepSoar() {
		
		WMESet sensors = super.stepSoar();	
		sensors = markupWMESetWithZigguratReward(sensors);
		nu_direction = sensors.getAttr("dir").getStr();
		return sensors;
	}

	/**
	 * Print the state as we do in Ziggurat.FlipPredictEnvironment
	 * @param cmd the command id issued: 0 = predict-no, 1 = predict-yes
	 */
	@Override
	protected void printState(int cmd) {

		if (nu_state.equals("A")) System.out.print("In State ONE ");
		else if (nu_state.equals("B")) System.out.print("In State TWO ");
		else System.out.print("In State UNKNOWN!! ");

		System.out.print(" moving " + nu_direction);

		System.out.println(" expecting reward of " + cmd);

		// we're going to "model" what's happening in the
		// actual soar environment here...
		// NOTE that we're not actually changing the agent
		// in any way based on this -- we just can't easily communicate
		// with the environment itself, so we have no simple
		// way to print out what state the agent is in and whether
		// its prediction is good without mirroring the environment here....

		int actualOutcome = 0;
		if ( nu_state.equals("A") && nu_direction.equals("right") )
		{
			nu_state = "B";
			actualOutcome = 1;
		}
		else if ( nu_state.equals("B") && nu_direction.equals("left") )
		{
			nu_state = "A";
			actualOutcome = 1;
		}

		//Calculate a new direction for the agent
		//Calculate the agent's new reward sensors
		if (cmd == actualOutcome)
		{
			System.out.println("predict error 0 (listener side, this is mirrored with actual functionality in Soar env)");
		}
		else
		{
			System.out.println("predict error 1 (listener side, this is mirrored with actual functionality in Soar env)");
		}



	}

	/**
	 * @return 2: predict-yes, predict-no
	 */
	@Override
	public int getNumCommands() {
		return 2;
	}
	/**
	 * converts an episode into a unique combination of three characters
	 * 1 or 0   - reward
	 * L,R or U - direction
	 * 1 or 0   - predicted outcome
	 */
	public String stringify(ElementalEpisode elEp)
	{
		//Calculte the reward character
		String sensorString = "0";
		if (elEp.getSensors().getAttr(WME.REWARD_STRING).getDouble() > 0.0)
		{
			sensorString = "1";
		}

		//Calculate the direction character

		String d = elEp.getSensors().getAttr("dir").getStr();
		sensorString += d.toUpperCase().charAt(0);

		//append the command and return
		return sensorString + stringify(elEp.getCommand());
	}//stringify ElementalEpisode

	public String stringify(int cmd)
	{
		if ((cmd < 0) || (cmd > 1)) return "?";

		return "" + cmd;
	}


}
