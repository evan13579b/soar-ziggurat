package Ziggurat;

/**
 * The ZigguClone, does what normal Ziggurat would do, except that it 
 * reads action selections from an array... in this way, we can duplicate
 * the action selections, e.g., performed by Soar.
 * 
 * @author wallaces
 *
 */
public class ZigguClone extends Ziggurat {

	int[] actions = {1,1,0,0,0,1,100,100};
	int nActions = 0;
	
	public ZigguClone(Environment env)
	{
		super(env);
	}

	
	 protected int chooseCommand() {
		 this.mon.enter("ZigguClone.chooseCommand()");
		 int cmd = super.chooseCommand();
		 int knowncmd = actions[nActions++];
		 
		 this.mon.log("Ziggurat.chooseCommand() selected:" + cmd + " but action array says do: " + knowncmd);
		 this.mon.exit("ZigguClone.chooseCommand()");
		 
		 return knowncmd;
		 
	 }

}
