package se;

import java.util.Random;

import sml.Agent;
import sml.Identifier;
import sml.IntElement;
import sml.Kernel;
import sml.StringElement;
import sml.WMElement;
import sml.smlRunEventId;
import sml.smlUpdateEventId;

/**
 * 
 * @author evan (with some copying of wallace's code)
 *
 */
public class SoarEnvironment implements Agent.RunEventInterface, Kernel.UpdateEventInterface {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SoarEnvironment soarEnvironment=new SoarEnvironment(env.Environment.FLIP,(new Random()).nextInt());
		soarEnvironment.start();
		
		while(true){
			try {
				//System.out.println("SoarEvironment running...");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private Kernel ncb_kernel;
	private Agent ncb_agent;
	private env.Environment environment;
	private Random random;

	private IntElement rewardWME;
	private StringElement actionWME;
	private StringElement observationWME;
	
	private String previousObservation="-1";
	
	public SoarEnvironment(env.Environment environment,int seed){
		this.environment=environment;
		
		this.random=new Random(seed);
		
		environment.setRandomState(random);
	}
	
	public void start(){
		ncb_kernel = Kernel.CreateRemoteConnection();
		ncb_kernel.RegisterForUpdateEvent(smlUpdateEventId.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, this, null);
		
		ncb_agent=ncb_kernel.GetAgentByIndex(0);
		
		this.createWMEs(ncb_agent);
	}

	private void createWMEs(Agent agent){
		Identifier inputLink=agent.GetInputLink();
		this.rewardWME=inputLink.CreateIntWME("reward", 0);

		String action=environment.getRandomAction(random);
		System.out.println("taking action "+action);
		
		this.actionWME=inputLink.CreateStringWME("action", action);		
		
		String observation=environment.takeAction(action);
		System.out.println("observation is "+observation);
		
		this.previousObservation=observation;
		
		this.observationWME=inputLink.CreateStringWME("observation", "");
		
		agent.Commit();
	}
	
	@Override
	public void runEventHandler(int eventID, Object arg1, Agent agent, int arg3) {
		if (eventID == smlRunEventId.smlEVENT_BEFORE_INPUT_PHASE.swigValue()) {
			
		}
	}
	
	private void updateWMEs(Agent agent){
		String action=environment.getRandomAction(random);
		
		
		int reward=0;
		WMElement outputWME=agent.GetOutputLink().FindByAttribute("output", 0);

		if(outputWME!=null){
			WMElement predictionWME=outputWME.ConvertToIdentifier().FindByAttribute("prediction", 0);
			String prediction=predictionWME.GetValueAsString();
			System.out.println("prediction is "+prediction);
			if(prediction.equals(this.previousObservation)){
				
				reward=1;
			}
		}
		System.out.println("reward is "+reward);
		agent.GetCommand(0).AddStatusComplete();
		System.out.println("taking action "+action);
		agent.Update(rewardWME, reward);
		agent.Update(actionWME, action);
		agent.Update(observationWME, this.previousObservation);
		String observation=environment.takeAction(action);
		System.out.println("observation is "+observation);
		this.previousObservation=observation;
		
		agent.Commit();
	}

	@Override
	public void updateEventHandler(int eventID, Object data, Kernel kernel, int runFlags) {
		Agent agent=kernel.GetAgentByIndex(0);
		
		updateWMEs(agent);
	}
}
