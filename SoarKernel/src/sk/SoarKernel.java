package sk;

import java.util.Random;


import sml.Agent;
import sml.Kernel;
import sml.smlSystemEventId;

public class SoarKernel {
	public static final String[] defaultSoarFilenames={"../SoarKernel/soarSrc/flip_predict.soar"};
	
	public static void main(String[] argv){
		SoarKernel soarKernel=new SoarKernel(defaultSoarFilenames,(new Random()).nextInt());
		soarKernel.listen();
		while(true){
			try {
				//System.out.println("SoarKernel running...");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private String[] soarFilenames;
	private Kernel kernel;
	private int seed;
	
	public SoarKernel(String[] soarFilenames,int seed){
		this.soarFilenames=soarFilenames;
		this.seed=seed;
	}
	
	public void listen(){
		kernel=Kernel.CreateKernelInNewThread();

		Agent agent=kernel.CreateAgent("kernel");
		for(String filename:soarFilenames){
			agent.ExecuteCommandLine("source "+filename);
		}
		agent.ExecuteCommandLine("srand "+this.seed);
	}
}
