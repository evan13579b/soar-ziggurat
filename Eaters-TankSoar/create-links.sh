
SOAR_OUT=../Soar-Suite/out

if [ -e $SOAR_OUT ]
  then
  cd lib
  for a in `ls ../${SOAR_OUT}/java/*.jar`
   do
    echo "linking $a"
    ln -sf $a
  done
  cd ..

  if ! [ -e bin ] 
   then
    mkdir bin
    cd bin
    ln -sf ../lib java
    ln -sf ../${SOAR_OUT}/SoarJavaDebugger.jar 
    ln -sf ../${SOAR_OUT}/libJava_sml_ClientInterface.jnilib
    ln -sf ../${SOAR_OUT}/libSoar.dylib
  fi
else
  echo "${SOAR_OUT} doesn't exist...."
fi