#!/bin/bash

export SOAR_HOME="$(dirname "$0")/bin"
export DYLD_LIBRARY_PATH="$SOAR_HOME"
cd $(dirname "$0")

DEFAULT_CFG=config/eaters.cnf

CONFIG=${1:-$DEFAULT_CFG}
echo CONFIG: $CONFIG
java -XstartOnFirstThread -jar "$SOAR_HOME/Eaters_TankSoar.jar" ${CONFIG} &

