package sl;

import java.util.ArrayList;
import java.util.Hashtable;

import sml.Agent;

/**
 * A FPSL interface that will specialize Soar rules using a tree of templates.
 * This is what we did for the AAAI-SS paper.
 * 
 * @author wallaces
 *
 */
public class TemplateTree<E extends env.Episode> extends StateEstimator<E> {

	private static final float varianceThreshold=0.10f;
	private static final int timesUsedThreshold=0;
	
	public ArrayList<Hashtable<String,String>> getProductionsToSpecialize(ArrayList<Hashtable<String,String>> parsedLines)
	{


		ArrayList<Hashtable<String,String>> needsSpecialization=new ArrayList<Hashtable<String,String>>();
		float timesUsed;
		float rewardVariance;

		for(Hashtable<String,String> line:parsedLines){
			timesUsed=Float.parseFloat(line.get("TimesUsed"));
			if(line.get("Name").contains("rvt") && !rulesAlreadySpecialized.contains(line.get("Name")) && timesUsed>=TemplateTree.timesUsedThreshold){
				System.out.println(line.get("Name") + " " + line.get("RewardVariance"));
				rewardVariance=Float.parseFloat(line.get("RewardVariance"));
				if(rewardVariance>TemplateTree.varianceThreshold){
					needsSpecialization.add(line);
				}
			}
		}
		return needsSpecialization;
	}
	private int parseNameForLevel(String name){
		String[] parts=name.split("\\*");
		int lastIndex=parts.length-1;
		
		String levelPart=parts[lastIndex-1];
		String level=levelPart.substring(1);
		
		return Integer.parseInt(level);
	}
	public int specializeProduction(Hashtable<String,String> production,Agent agent){
		String name=production.get("Name"); 
		rulesAlreadySpecialized.add(name); // add this to the set of already specialized rules so we don't keep specializing the same rules
		
		String source=agent.ExecuteCommandLine("print "+name);
		int currentLevel=parseNameForLevel(name);
		int nextLevel=currentLevel+1;
		
		String newSource=createNewTemplate(source,name,nextLevel);
		/*
		for(String line:source.split("\n")){
			System.out.println("oldSource:"+line);
		}
		for(String line:newSource.split("\n")){
			System.out.println("newSource:"+line);
		}
		*/
		//agent.ExecuteCommandLine("excise "+name);
		agent.ExecuteCommandLine(newSource);
		
		return nextLevel;
	}
	public String[] getNewHashCodes(ArrayList<E> episodes,int maxLevel){
		if(maxLevel>episodes.size()){
			System.out.println("ERROR: level too large for the number of episodes processed");
			return null;
		}
		
		int lastEpisodeIndex=episodes.size()-1;
		String[] newHashCodes=new String[maxLevel+1];
		String hashCode="";
		
		String episodeString="";
		newHashCodes[0]=episodeString;//.hashCode();
		for(int episodeNumber=lastEpisodeIndex, currentLevel=1;episodeNumber>=lastEpisodeIndex-maxLevel+1;episodeNumber--,currentLevel++){
			episodeString=episodes.get(episodeNumber).toString();
			hashCode=episodeString;//.hashCode();
			newHashCodes[currentLevel]=hashCode;
			System.out.println("episodes->hash: "+episodeString+" -> "+hashCode);
		}
		return newHashCodes;
	}

	private String createNewTemplate(String source,String name,int level){
		String[] lines=source.split("\n");
		int arrowIndex=0;
		for(int i=0;i<lines.length;i++){
			if(lines[i].trim().equals("-->")){
				arrowIndex=i;
			}
		}
		assert(arrowIndex!=0);

		// we have to get rid of the starting 'rl*' or else we eventually get a screen of nothing but rl*rl*rl*rl*rl so this line
		// has to be entirely recreated.
		String newName=name.substring(3)+"*H"+level;
		String newFirstLine="sp {"+newName+"\n";
		String templateLine=":template\n";
		for(int i=1;i<lines.length;i++){
			lines[i]+="\n";
		}
		String newSource="";
		newSource+=newFirstLine;
		newSource+=templateLine;
		for(int i=1;i<arrowIndex;i++){
			newSource+=lines[i];
		}
		String newHashLine="(<i2> ^level-"+level+" <level-"+level+">)\n";
		newSource+=newHashLine;
		for(int i=arrowIndex;i<lines.length;i++){
			newSource+=lines[i];
		}

		return newSource;

	}
	
}


