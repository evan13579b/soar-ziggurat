package sl;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * A Runnable SoarListener with episodes that encode information from 
 * a 'Flip-like' environment
 * 
 * @author wallaces
 *
 */
public class FPSL {
	/**
	 * An Episode for a Flip-like environment, this makes things efficient.
	 */
	static class FPEpisode implements env.Episode {
		String action;
		String observation;

		FPEpisode(String a, String o) {
			action = a;
			observation = o;
		}
		public String getAction() { return action; }
		public String getObservation() { return observation; }
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			else if (!(other instanceof FPEpisode)) {
				return false;
			}
			FPEpisode ofpe = (FPEpisode)other;
			return this.action.equals(ofpe.action) && this.observation.equals(ofpe.observation);

		}
		public String toString() {return action+observation;}
		public int hashCode() { return action.hashCode()+observation.hashCode(); }
	}


	/**
	 * A builder to efficiently create an episode from WMETrees
	 */
	static class FPEpisodeBuilder implements SL.EpisodeFactory<FPEpisode> {

		@Override
		public FPEpisode buildEpisode(WMETree il, WMETree ol) {
			// take the old 'action' value off the output link  and...
			// take the new 'observation' value off the input link
			if (il==null || ol==null) return null;
			System.out.println("BUILDING");
			System.out.println(ol.toString());
			System.out.println(il.toString());

			FPEpisode fpe = new FPEpisode(ol.get("action"), il.get("observation"));
			System.out.println("BUILT: " + fpe.toString());
			return fpe;
		}

	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String [] args) {
		if (args.length < 2) {
			System.err.println("Usage: FPSL " + " <decisions> <historyfile> [estimator class]");
			System.exit(1);
		}

		int dc = Integer.parseInt(args[0]);
		String hf = args[1];
		String est = "sl.TemplateTree";
		if (args.length > 2 ) {
			est = args[2];
		}


		StateEstimator<FPEpisode> se = null;
		try {
			Class cls = Class.forName(est);
			Object seo = cls.newInstance();
			se = (StateEstimator<FPEpisode>)seo;
			System.out.println("Successfully Created StateEstimator: " + est);

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("Connecting...");
		SL<FPEpisode> sl = new SL<FPEpisode>(se, new FPEpisodeBuilder());
		System.out.println("Running " + dc);

		sl.run(dc);
		System.out.println("Saving history to " + hf);
		try {
			FileOutputStream hfs = new FileOutputStream(hf);
			sl.showHistory(new PrintStream(hfs));
		}
		catch (IOException e) {
			System.out.println("Couldn't save history!");
			e.printStackTrace();
		}

		System.out.println("Exiting");
		System.exit(0);
	}


}