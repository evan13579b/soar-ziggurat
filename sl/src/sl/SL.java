package sl;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;



import sml.Agent;
import sml.Kernel;
import sml.StringElement;
import sml.smlRunEventId;


/**
 * A SoarListener
 * 
 * This listener observes an agent interact with its environment and creates an alternate
 * episodic memory by observing agent's input and output link.
 * 
 * The listener will use its observations to uncover new information about the environment's
 * state space. The current use is to identify hidden states. For this information to be useful
 * to the agent, this listener must pass data back to the agent that augments the agent's own
 * sensors. 
 * 
 * Using the SL:
 * 
 *  1) Start by creating a subclass that for a specific domain @see FPSL
 *  2) The subclass may want to create its own Episode and EpisodeFactory
 *  3) Create a main method for the subclass that creates an SL instance
 *      using a specific Episode/EpisodeFactory and runs the agent 
 * 
 * @author wallaces
 *
 * @param <E>
 */
public class SL<E extends env.Episode> implements Agent.RunEventInterface {
		
	static interface EpisodeFactory<P extends env.Episode> {
		public P buildEpisode(WMETree il, WMETree ol);
	}
	
	private WMETree mt_inputLinkTree;
	private WMETree mt_outputLinkTree;

	
	protected ArrayList<E> episodes;
	

	// The level for our learner, initially at zero
	private int level=0;
	ArrayList<StringElement> levelHashWMEs=new ArrayList<StringElement>();
	

	StateEstimator<E> estimator;
	EpisodeFactory<E> episodeFactory;
	
	// Thread local variables:
	// ncb_  not on callbacks
	Kernel ncb_kernel;
	Agent ncb_agent;

	
	public SL(StateEstimator<E> e, EpisodeFactory<E> f) {		
		ncb_kernel = Kernel.CreateRemoteConnection();
		if (ncb_kernel.HadError()) {
			System.err.println("Couldn't connect to a remote kernel, is the environment running?");
			System.err.println(ncb_kernel.GetLastErrorDescription());
			System.exit(0);
		}
		episodes = new ArrayList<E>(3000);
		estimator = e;
		episodeFactory = f;
		e.setListener(this);
		System.out.println("Connected to environment!");
		ncb_agent = ncb_kernel.GetAgentByIndex(0);
		
		// The environment will read the output link sometime during the output phase 
		// and possibly write to the input and output links at the same time.
		
		// we want to capture the information on the output link before the environment gets it
		ncb_agent.RegisterForRunEvent(smlRunEventId.smlEVENT_BEFORE_OUTPUT_PHASE, this, null);
		// also, the new information from the environment doesn't seem to be visible until
		// AFTER the input phase, so we need to capture that in this call back...
		ncb_agent.RegisterForRunEvent(smlRunEventId.smlEVENT_AFTER_INPUT_PHASE, this, null);
		
		
		System.out.println("Listening to the stream from agent: '" + ncb_agent.GetAgentName() + "'...");
		mt_inputLinkTree = null;
		mt_outputLinkTree = null;
		this.levelHashWMEs.add(null); //adding this to make indexing of levelHashes more natural
		
	}
	
	

	public void run(int n) {
		int i = n;
		while (i > 0) {
			ncb_agent.RunSelfTilOutput(); // this should block.
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			estimator.step();
			i--;
		}

	}
	
	
	@Override
	public void runEventHandler(int eventID, Object arg1, Agent agent, int arg3) {
		if( eventID == smlRunEventId.smlEVENT_BEFORE_OUTPUT_PHASE.swigValue()) {
			beforeOutputPhase(agent);
		}
		else if (eventID == smlRunEventId.smlEVENT_AFTER_INPUT_PHASE.swigValue()) {
			System.out.println("DECISION: " + agent.GetDecisionCycleCounter());
			afterInputPhase(agent);
		}
	}
	private void afterInputPhase(Agent a) {
		// get the state..
		String state = a.ExecuteCommandLine("print --depth 3 --internal i2").trim();
		System.out.println("reading tokens from input link data...");
		for(String z : state.split("[\r\n]+")) {
			System.out.println( "  - " + z);
		}
		System.out.println("After input phase for DC: " + a.GetDecisionCycleCounter());
		synchronized(this) {
//			System.out.println(" mt state: " + state);
			mt_inputLinkTree = WMETree.parseString("I2", state);
			E newEpisode = episodeFactory.buildEpisode(mt_inputLinkTree, mt_outputLinkTree);
			if (newEpisode != null) {
				episodes.add(newEpisode);
				estimator.episodeAdded(newEpisode);
				System.out.println("built episode for previous decision");
			}
		}
		//System.out.println("putting zig on il");
		//a.GetInputLink().CreateIntWME("zig", 10);
		

		// grab the print --rl text, and parse each line getting the individual components
		// and then filter it by rules that have been ran at least 'SoarListenerEnvironment.timesUsedThreshold' times 
		// and have a variance greater than 'SoarListenerEnvironment.varianceThreshold'
		

		int newLevel=0;
		long start = System.nanoTime();
		String printRLString=a.ExecuteCommandLine("print --rl");
		long stop = System.nanoTime();
		System.out.println("Getting RL took: " + (stop-start)/1000000);
		if (!printRLString.trim().isEmpty()) {
		ArrayList<Hashtable<String,String>> parsedLines=parsePrintRL(printRLString);
		ArrayList<Hashtable<String,String>> overgeneralList=estimator.getProductionsToSpecialize(parsedLines);
		System.out.println(overgeneralList.size() + " Productions to specialize at DC " + a.GetDecisionCycleCounter());
		for (Hashtable<String,String> e : overgeneralList) {
			System.out.println(" - " + e.get("Name"));
		}
		newLevel=specializeProductions(overgeneralList,a);	
		
		assert(newLevel<=level+1);
		
		if(newLevel>level){
			level=newLevel;
			levelHashWMEs.add(a.GetInputLink().CreateStringWME("level-"+level,""));
		}
		
		String [] hashCodes=estimator.getNewHashCodes(episodes,level);
		sendHashCodesToSoar(a,hashCodes);
		
		}
		else {
			System.out.println("RL String is EMPTY");
			if (a.GetDecisionCycleCounter() != 1)
				System.err.println("WARNING");
		}
		stop = System.nanoTime();
		System.out.println("full RL section all took:" + (stop-start)/1000000);
		a.Commit();
		

	}
	private void beforeOutputPhase(Agent a) {
		
		// grab the output link data....
		String action = a.ExecuteCommandLine("print --depth 3 --internal i3").trim();
		System.out.println("reading tokens from output link data...");
		for(String z : action.split("[\r\n]+")) {
			System.out.println( "  - " + z);
		}

		/*
		String lr = a.ExecuteCommandLine("print --depth 3 --internal r1").trim();
		System.out.println("reading tokens from reward link (for previous action)... ");
		for(String z : lr.split("[\r\n]+")) {
			System.out.println( "  - " + z);
		}
	*/
		synchronized(this) {
//			System.out.println(" mt action: " + action);
			mt_outputLinkTree = WMETree.parseString("I3", action);
		}

	}
	

	private String[] splitIntoLines(String text){
		return text.split("\n");
	}
	
	private String[] splitBySpaces(String text){
		return text.trim().split(" ");
	}
	
	private Hashtable<String,String> parsePrintRLLine(String printRLLineString){
		Hashtable<String,String> resultsMap=new Hashtable<String,String>();
		String[] parts=splitBySpaces(printRLLineString);
		if(parts.length!=6){
			System.out.println("print --rl did not have the right number of columns ("+parts.length+")");
		}
		else{
			resultsMap.put("Name", parts[0]);
			resultsMap.put("TimesUsed", parts[2]);
			resultsMap.put("Q-Value", parts[3]);
			resultsMap.put("RewardMean", parts[4]);
			resultsMap.put("RewardVariance",parts[5]);
		}
		return resultsMap;
	}
	
	ArrayList<Hashtable<String,String>> parsePrintRL(String printRLString){
		boolean verbose=false;
		
		ArrayList<Hashtable<String,String>> resultsArray=new ArrayList<Hashtable<String,String>>();
		String[] lines=splitIntoLines(printRLString);
		for(String line:lines){
			Hashtable<String,String> parsedPrintRLLine=parsePrintRLLine(line);
			resultsArray.add(parsedPrintRLLine);
			
			if(verbose){
				System.out.println("line is:"+line);
				System.out.println("Name:"+parsedPrintRLLine.get("Name"));
				System.out.println("TimesUsed:"+parsedPrintRLLine.get("TimesUsed"));
				System.out.println("Q-Value:"+parsedPrintRLLine.get("Q-Value"));
				System.out.println("RewardMean:"+parsedPrintRLLine.get("RewardMean"));
				System.out.println("RewardVariance:"+parsedPrintRLLine.get("RewardVariance"));
			}
		}
		return resultsArray;
	}
	
	public void showHistory(PrintStream s) {
		for(E e : episodes ) {
			s.println(e.toString());
		}

	}

	private int specializeProductions(ArrayList<Hashtable<String,String>> overgeneralList,Agent agent){
		int level,maxLevel=0;
		for(Hashtable<String,String> overgeneralProduction:overgeneralList){
			level=estimator.specializeProduction(overgeneralProduction,agent);
			if(level>maxLevel){
				maxLevel=level;
			}
		}
		return maxLevel;
	}
	
	private void sendHashCodesToSoar(Agent agent,String[] hashCodes){
		for(int i=1;i<hashCodes.length;i++){
			agent.Update(this.levelHashWMEs.get(i), hashCodes[i]);
		}
		agent.Commit();
	}
	
	
	


}
