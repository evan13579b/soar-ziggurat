package sl;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WMETree {
	HashSet<Edge> edges;
	
	static class Edge {
		String attribute;
		String value;
		
		Edge(String a, String v) {
			attribute = a;
			value = v;
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof Edge)) return false;
			if (!(((Edge)o).attribute.equals(attribute))) return false;
			if (!(((Edge)o).value.equals(value))) return false;
			return true;
		}
		public int hashCode() { 
			return attribute.hashCode()^value.hashCode();
		}
		public String toString() {
			return "(^"+attribute + ' ' + value + ')';
		}
	}
	private static Pattern SOAR_WME_PATTERN = Pattern.compile("\\((\\d+): (\\w+) \\^(\\S+) (\\S+)\\)");
	private static Pattern SOAR_ID_PATTERN = Pattern.compile("[A-Z]\\d+");
	public static boolean isID(String token) {
		return SOAR_ID_PATTERN.matcher(token).matches();
	}
	private WMETree(int sz) {
		edges = new HashSet<Edge>(sz);
	}
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		for (Edge e: edges) {
			sb.append(e.toString());
		}
		return sb.toString();
	}
	public String get(String attributePath){
		for (Edge e : edges) {
			if (e.attribute.equals(attributePath)) return e.value;
		}
		return null;
	}
	public static WMETree parseString(String rootID, String internalWMEs) {
		WMETree wt = new WMETree(2);
		if (rootID.equals("I3")) {
			if (internalWMEs.contains("dir U")) wt.edges.add(new Edge("dir", "U"));
			if (internalWMEs.contains("dir L")) wt.edges.add(new Edge("dir", "L"));
			if (internalWMEs.contains("dir R")) wt.edges.add(new Edge("dir", "R"));
		}
		else if (rootID.equals("I2")) {
			if (internalWMEs.contains("see 0")) wt.edges.add(new Edge("see", "0"));
			if (internalWMEs.contains("see 1")) wt.edges.add(new Edge("see", "1"));
		}
		else { System.err.println("Impossible!");
		}
		return wt;
		
	}
	public static WMETree wasparseString(String rootID, String internalWMEs) {
			
		if (internalWMEs == null) return null;
		WMETree wt = new WMETree(20);
			 HashMap<String, LinkedList<Edge>> parentToChild = new HashMap<String, LinkedList<Edge>>(40);
			 HashMap<String, LinkedList<Edge>> leaves = new HashMap<String, LinkedList<Edge>>(30);
			
			Matcher m = SOAR_WME_PATTERN.matcher(internalWMEs);

			String tt, root, attr, val;
			int lastMatch = 0;

			while( m.find(lastMatch) ) {
				tt = m.group(1);
				root = m.group(2);
				attr = m.group(3);
				val = m.group(4);
				lastMatch = m.end();

			//	System.out.println("Found: " + root + " ^" + attr + " " + val);
				Edge newEdge = new Edge(attr,val);
				if (isID(val)) {
					// the root has a child...
					LinkedList<Edge> children = parentToChild.get(root);
					if (children == null) {
						children = new LinkedList<Edge>();
						parentToChild.put(root, children);
					}
					children.add(newEdge);
				}
				else {
					LinkedList<Edge> rootleaves;
					rootleaves = leaves.get(root);
					if (rootleaves == null) {
						rootleaves = new LinkedList<Edge>();
						leaves.put(root, rootleaves);
				//		System.out.println("Putting leaf off of " + root);
					}
					rootleaves.add(newEdge);
				}
			
			}
			if (parentToChild.size() > 0) {
				System.err.println("WARNING: Building Nodes is unimplemented..." + parentToChild.size());
				//System.exit(1);
			}
			//System.out.println("Getting leaves for " + rootID);
			if (leaves.get(rootID) != null) {
			for (Edge e: leaves.get(rootID)) {
				wt.edges.add(e);
			}
			}
			return wt;

		
		
	}
}
