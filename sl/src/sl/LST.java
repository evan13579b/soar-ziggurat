package sl;

import java.util.ArrayList;
import java.util.Hashtable;


import sml.Agent;

public class LST<E extends env.Episode> extends StateEstimator<E> {

	static final float varianceThreshold=0.10f;
	static final int timesUsedThreshold=0;
	lst.LST<E> lst = null;
	boolean createTemplates = false;
	int templateVersion = 0;
	int steps = 0;
	// a list of the templates that have been asserted.
	ArrayList<String> templates = new ArrayList<String>(10);
	
	public ArrayList<Hashtable<String,String>> getProductionsToSpecialize(ArrayList<Hashtable<String,String>> parsedLines)
	{


		ArrayList<Hashtable<String,String>> needsSpecialization=new ArrayList<Hashtable<String,String>>();
		float timesUsed;
		float rewardVariance;
	
		if (createTemplates) {
			for(Hashtable<String,String> line:parsedLines){
				timesUsed=Float.parseFloat(line.get("TimesUsed"));
				if(line.get("Name").contains("rvt") && !rulesAlreadySpecialized.contains(line.get("Name")) && timesUsed>=LST.timesUsedThreshold){
					//System.out.println(line.get("Name") + " " + line.get("RewardVariance"));
					rewardVariance=Float.parseFloat(line.get("RewardVariance"));
					if(rewardVariance>LST.varianceThreshold){
						needsSpecialization.add(line);
					}
				}
			}
		createTemplates = false;	
		}

		return needsSpecialization;
	}
	private int parseNameForLevel(String name){
		String[] parts=name.split("\\*");
		int lastIndex=parts.length-1;
		
		String levelPart=parts[lastIndex-1];
		String level=levelPart.substring(1);
		
		return Integer.parseInt(level);
	}
	public int specializeProduction(Hashtable<String,String> production,Agent agent){
		String name=production.get("Name"); 
		rulesAlreadySpecialized.add(name); // add this to the set of already specialized rules so we don't keep specializing the same rules
		
		String source=agent.ExecuteCommandLine("print "+name);
		int currentLevel=parseNameForLevel(name);
		int nextLevel=currentLevel+1;
		
		String newSource=createNewTemplate(source,name,nextLevel);
		/*
		for(String line:source.split("\n")){
			System.out.println("oldSource:"+line);
		}
		for(String line:newSource.split("\n")){
			System.out.println("newSource:"+line);
		}
		*/
		//agent.ExecuteCommandLine("excise "+name);
		agent.ExecuteCommandLine(newSource);
		
		return nextLevel;
	}
	public String[] getNewHashCodes(ArrayList<E> episodes,int maxLevel){
		if(maxLevel>episodes.size()){
			System.out.println("ERROR: level too large for the number of episodes processed");
			return null;
		}
		System.err.println("maxLevel = " + maxLevel);
		assert maxLevel <= 1;
		
		String[] newHashCodes=new String[maxLevel+1];
		newHashCodes[0] = "n/a";
		
		if (maxLevel == 1) {
			newHashCodes[1] = lst.getLSTNode(episodes, true);
			System.out.print("Recent History is: ("+episodes.size()+") ");
			for(int i = 1; i <= 10; i++) {
				System.out.print(episodes.get(episodes.size()-i).toString());
				System.out.print('-');
			}
			System.out.println("");
			System.out.println("LST Hash says: " + newHashCodes[1]);
		}
		return newHashCodes;
	}

	private String createNewTemplate(String source,String name,int level){
		String[] lines=source.split("\n");
		int arrowIndex=0;
		for(int i=0;i<lines.length;i++){
			if(lines[i].trim().equals("-->")){
				arrowIndex=i;
			}
		}
		assert(arrowIndex!=0);

		// we have to get rid of the starting 'rl*' or else we eventually get a screen of nothing but rl*rl*rl*rl*rl so this line
		// has to be entirely recreated.
		
		String newName;
		if (templateVersion == 0) {
			newName =name.substring(3)+"*H"+level;
		} else {
			newName = name.substring(3)+"*v"+Integer.toString(templateVersion)+"*H"+level;
		}
		String newFirstLine="sp {"+newName+"\n";
		String templateLine=":template\n";
		for(int i=1;i<lines.length;i++){
			lines[i]+="\n";
		}
		String newSource="";
		newSource+=newFirstLine;
		newSource+=templateLine;
		for(int i=1;i<arrowIndex;i++){
			newSource+=lines[i];
		}
		String newHashLine="(<i2> ^level-"+level+" <level-"+level+">)\n";
		newSource+=newHashLine;
		for(int i=arrowIndex;i<lines.length;i++){
			newSource+=lines[i];
		}

		System.out.println("New Template: ");
		System.out.println(newSource);
		templates.add(newName);
		return newSource;

	}
	
	public void step() {
		steps++;
		if (steps == 1) {
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection --epsilon-greedy");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection -a on");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection --epsilon .3");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection -p epsilon linear");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection -r epsilon linear 0.0006");
		}
		if (steps == 200) {
			System.out.println("Building LST! (alg3EvanInterpretation)");
			
			lst.LST<E> a_lst=new lst.LST<E>(listener.episodes,3,"alg3EvanInterpretation",true);
			a_lst.print(true);
			lst = a_lst;
			this.createTemplates = true;
			System.out.println("Set LST!  Running 1 set to create templates......");
			lst.createDotFile("lst_lst-"+steps+".dot");
		}
	}
}
