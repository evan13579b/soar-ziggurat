package sl;

import java.util.ArrayList;
import java.util.Hashtable;


/**
 * The ReLST builds a LST for the environment at specified intervals 
 * according to the following rules:
 * 
 *  1) if there is no LST, build one at the next interval
 *  2) if the current LST has made a bad prediction, build a new one at the next interval
 *  
 * Rules in the agents  
 * @author wallaces
 *
 * @param <E>
 */
public class ReLST<E extends env.Episode> extends LST<E> {
	/** the intervals at which to (potentially) build an LST */
	int[] lst_epochs = {50,100,200,300,500,950,1000,1500, 2500};

	
	int next_epoch = 0;
	boolean makeLSTAtEpoch = true;
	
	public void episodeAdded(E newEpisode) {
		if (lst==null) return;
		
		// check if the lst will correctly predict the current episode...
		E last = listener.episodes.remove(listener.episodes.size() - 1);
		String lstPrediction = lst.predictResult(last.getAction(), listener.episodes, true);
		if (lstPrediction == null || !lstPrediction.equals(last.getObservation())) {
			System.out.println("LST Does not predict: " + last.getObservation() + " for action " + last.getAction());
			makeLSTAtEpoch = true;
		}
		listener.episodes.add(last);
		
	}
	public ArrayList<Hashtable<String,String>> getProductionsToSpecialize(ArrayList<Hashtable<String,String>> parsedLines)
	{


		ArrayList<Hashtable<String,String>> needsSpecialization=new ArrayList<Hashtable<String,String>>();
		float timesUsed;
		float rewardVariance;
	
		System.out.println("Current Templates: " + templates.size());
		for (String t : templates) 
			System.out.println( "  - "+t);
				
		if (templates.size() > 1) {
			for(Hashtable<String,String> line:parsedLines){
				timesUsed=Float.parseFloat(line.get("TimesUsed"));
				if(line.get("Name").contains(templates.get(0)) && timesUsed>=LST.timesUsedThreshold){
				//	System.out.println(line.get("Name") + " " + line.get("RewardVariance"));
					rewardVariance=Float.parseFloat(line.get("RewardVariance"));
					if(rewardVariance>LST.varianceThreshold){
						System.out.println("LST RULE HAS VARIANCE! Will BUILD LST AT NEXT EPOCH");
						makeLSTAtEpoch = true;
					}
				}
			}
			
		}
		// 
		if (createTemplates) {
			for(Hashtable<String,String> line:parsedLines){
				timesUsed=Float.parseFloat(line.get("TimesUsed"));
				if(line.get("Name").contains("rvt") && !rulesAlreadySpecialized.contains(line.get("Name")) && timesUsed>=LST.timesUsedThreshold){
					System.out.println(line.get("Name") + " " + line.get("RewardVariance"));
					rewardVariance=Float.parseFloat(line.get("RewardVariance"));
					if(rewardVariance>LST.varianceThreshold){
						needsSpecialization.add(line);
					}
				}
				
			}
			createTemplates = false;
			
		}

		return needsSpecialization;
	}
	public void step() {
		steps++;
		if (steps == 1) {
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection --epsilon-greedy");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection -a on");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection --epsilon .3");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection -p epsilon linear");
			listener.ncb_agent.ExecuteCommandLine("indifferent-selection -r epsilon linear 0.0006");
		}
		if (steps == lst_epochs[next_epoch]) {
			if (makeLSTAtEpoch) {
				// always build the first LST...

				if (templates.size() > 0) {
					System.out.println("LST EXISTS... must excise!");
					String printRLString=listener.ncb_agent.ExecuteCommandLine("print --rl");
					ArrayList<Hashtable<String,String>> parsedLines=listener.parsePrintRL(printRLString);

					for (String t : templates) {
						listener.ncb_agent.ExecuteCommandLine("excise "+t);
						System.out.println(" - excising " + t);
						for(Hashtable<String,String> line:parsedLines){
							if(line.get("Name").contains(t)){
								System.out.println("    - excising " + line.get("Name"));
								listener.ncb_agent.ExecuteCommandLine("excise "+line.get("Name"));
							}
						}

					}

					rulesAlreadySpecialized.clear();
					templateVersion += 1;
					templates.clear();
				}
				System.out.println("Building LST! (alg3 Evan) from " + listener.episodes.size() + " step history...");
				//LST<FPSL.FPEpisode> lst=new LST<FPSL.FPEpisode>(sl.episodes,3,"alg3WallaceInterpretation",true);
				lst.LST<E> alst=new lst.LST<E>(listener.episodes,3,"alg3EvanInterpretation",true);
				//LST<FPSL.FPEpisode> lst=new LST<FPSL.FPEpisode>(sl.episodes,3,"alg2",true);
				alst.print(true);
				lst = alst;
				createTemplates = true;
				System.out.println("Set LST!  Running 1 set to create templates......");
				lst.createDotFile("relst_lst-"+steps+".dot");
				listener.ncb_agent.ExecuteCommandLine("indifferent-selection --epsilon .3");
				listener.ncb_agent.ExecuteCommandLine("indifferent-selection -p epsilon linear");
				listener.ncb_agent.ExecuteCommandLine("indifferent-selection -r epsilon linear 0.0006");
				System.out.println("Bumped up alpha...");
			}

		}
		if (steps == lst_epochs[next_epoch]+1) {
			makeLSTAtEpoch = false;
			if (next_epoch < lst_epochs.length-1)
				next_epoch++;
		}
	}
}
