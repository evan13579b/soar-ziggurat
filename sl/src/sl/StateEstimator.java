package sl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;

import sml.Agent;

public abstract class StateEstimator<E extends env.Episode> {
	SL<E> listener;
	// set of names of rules we've already specialized
	HashSet<String> rulesAlreadySpecialized=new HashSet<String>();

	public void setListener(SL<E> sl) {
		listener = sl;
	}
	
	public void episodeAdded(E newEpisode) {}
	
	public abstract ArrayList<Hashtable<String,String>> getProductionsToSpecialize(ArrayList<Hashtable<String,String>> parsedLines);
	
	
	// this returns a list of hashcodes up to the given level
	public abstract String[] getNewHashCodes(ArrayList<E> episodes,int maxLevel);
	
	
	public abstract int specializeProduction(Hashtable<String,String> production,Agent agent);
	
	public void step() {
		
	}
}
