package exp;

import java.util.Random;

import se.SoarEnvironment;
import sk.SoarKernel;

public class Experimenter {
	static public void main(String[] argv){
		SoarKernel soarKernel=new SoarKernel(SoarKernel.defaultSoarFilenames,(new Random()).nextInt());
		soarKernel.listen();
		
		SoarEnvironment soarEnvironment=new SoarEnvironment(env.Environment.FLIP,(new Random()).nextInt());
		soarEnvironment.start();
		
		while(true){
			try {
				//System.out.println("SoarEvironment running...");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
