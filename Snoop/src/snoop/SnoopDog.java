package snoop;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.umich.soar.IntWme;

import sml.Agent;
import sml.ClientAnalyzedXML;
import sml.ClientXML;
import sml.Identifier;
import sml.IntElement;
import sml.Kernel;
import sml.smlUpdateEventId;

/**
 * SnoopDog: Listens to a Soar agent interact with an Environment via SML.
 * Assumes that:
 *  1) the environment is running on the same machine as SnoopDog
 *  2) the environment has only 1 agent
 *  
 * How to use:
 *  - Launch the environment
 *  - Launch SnoopDog
 *  - (optionally) Launch Soar Debugger, select Kernel->'Connect to Remote Soar' select default dialog options
 *  - (optionally) If using the Soar Debugger, set the "stop point" between output and input:
 *      set-stop-phase --before --input
 *      this will ensure that the debugger and SnoopDog are viewing the agent's memory at the same point in time
 *  - step through the environment with the debugger (or whatever)
 *  - NOTE that in ToH if you're stepping through with the debugger set as above, the environment look like
 *      it's a half phase ahead -- that's because it's not actually waiting the for input phase to update the world view...
 *      
 * @author wallaces
 *
 */
public class SnoopDog implements Runnable, Kernel.UpdateEventInterface {

	Kernel k;
	
	IntElement prevStateWME = null;   // we'll use this to send the 'previous state' to the agent.
	
	// this maps states (a set of features) to unique ids (integers)
	HashMap<Set<String>,Integer> state_id_map;
	HashMap<Integer, Set<String>> id_state_map;
	
	// this maps actions (a set of command features) to unique ids (integers)
	HashMap<Set<String>, Integer> action_id_map;
	HashMap<Integer, Set<String>> id_action_map;
	
	// this deserves some explination ;)
	// this maps state ids (values in the state_id_map) to maps
	// that contain actionids and their resulting stateids
	// note that currently I'm not tracting the probability of a transition...
	//     <stateid, HashMap<action, LinkedList<stateid'>>
	HashMap<Integer, HashMap<Integer,LinkedList<Integer>>> transition_map;
	
	ToHStateWMEFilter stateFilter = new ToHStateWMEFilter();
	OutputCommandWMEFilter commandFilter = new OutputCommandWMEFilter();
	Integer previousState = -1;
	
	public static void main(String [] args) {
		
		SnoopDog newDog = new SnoopDog();
		newDog.run();
	}
	
	public SnoopDog() {
		state_id_map = new HashMap<Set<String>,Integer>(500);
		id_state_map = new HashMap<Integer, Set<String>>(500);
		action_id_map = new HashMap<Set<String>,Integer>(500);
		id_action_map = new HashMap<Integer, Set<String>>(500);
		
		transition_map = new HashMap<Integer, HashMap<Integer, LinkedList<Integer>>>();
		
		k = Kernel.CreateRemoteConnection();
		if (k.HadError()) {
			System.err.println("Couldn't connect to a remote kernel, is the environment running?");
			System.err.println(k.GetLastErrorDescription());
			return;
		}
		System.out.println("Connected");
		
        k.RegisterForUpdateEvent(smlUpdateEventId.smlEVENT_AFTER_ALL_GENERATED_OUTPUT, this, null) ;

	}
	
    /** This method is called when the "after_all_output_phases" event fires, at which point we update the world */
	public void updateEventHandler(int eventID, Object data, Kernel kernel, int runFlags)
	{
		System.out.println("Snoop Dog!");
		try
		{
			Agent a = kernel.GetAgentByIndex(0);

			//ClientAnalyzedXML xml = new ClientAnalyzedXML();
			//a.ExecuteCommandLineXML("print --depth 3 i2", xml);
			//System.out.println("Got this xml: " + xml.GenerateXMLString(true, true));

			
			// get the state...
			String r = a.ExecuteCommandLine("print --depth 3 --internal i2");
			System.out.println("reading tokens from input link data...");
			Set<String> state = stateFilter.runFilter(r);

			// here's what we see as the state...
			System.out.println("The state contains: " + state.size() + " elements!");
			for( String s: state ) {
				System.out.println(" state: " + s);
			}
			// create a unique id for the state if we haven't seen it, otherwise retrieve its id
			Integer stateid = state_id_map.get(state);
			if (stateid != null) {
				System.out.println(" ** This is state ID: " + stateid + "  (" + state_id_map.size() + " known states) **");
			}
			else {
				System.out.println(" ** This state is new! (" + state_id_map.size() + " states previously seen) **");
				stateid = state_id_map.size();
				state_id_map.put(state, stateid);
				id_state_map.put(stateid, state);
			}
			
			// now do a similar thing for the action command....
			String o = a.ExecuteCommandLine("print --depth 3 --internal i3");
			System.out.println("reading tokens from output link data...");
			Set<String> cmd = commandFilter.runFilter(o);
			for (String s: cmd) {
				System.out.println(" cmd: " + s + " " + cmd.size());
			}

			if (cmd.size() > 0) {
				Integer actionid = action_id_map.get(cmd);
				if (actionid != null) {
					System.out.println(" ++ This is action ID " + actionid  + "  (" + action_id_map.size() + " known actions) ++");
				}
				else {
					System.out.println(" ++ This action is new! (" + action_id_map.size() + " actions previously seen) ++");
					actionid = 100 + action_id_map.size(); // add 100 so it's easily distinguished from a state id
					action_id_map.put(cmd, actionid);
					id_action_map.put(actionid, cmd);
				}
				// if there's a previous state, and an action, then there's a transition to map....
				// here we're assuming that transitions only occur due to the agent's actions
				// i.e., there are no exogenous events.
				if (previousState != -1) {
					recordTransition( previousState, actionid, stateid);
				}
			}
			previousState = stateid;

			// as proof of concept, inform the agent of the state transition
			if (prevStateWME != null) prevStateWME.DestroyWME();
			Identifier il = a.GetInputLink();
			prevStateWME = il.CreateIntWME("previous-state", previousState);
			
			if (state_id_map.size() == 9) {
				printEverythingIKnow();
			}
		} catch (Exception e) {
			System.err.println(e.toString());
			e.printStackTrace(System.err);
		}
	}

	boolean recordTransition( Integer previousState, Integer actionid, Integer stateid ) {
		// a transition occurred
		if (!transition_map.containsKey(previousState)) {
			transition_map.put(previousState, new HashMap<Integer, LinkedList<Integer>>(15));
		}
		HashMap<Integer, LinkedList<Integer>> action_state_map = transition_map.get(previousState);
		if (action_state_map.containsKey(actionid)) {
			System.out.println("I've done this action in this state before....");
			LinkedList<Integer> nextstates = action_state_map.get(actionid);
			if (nextstates.contains(stateid)) {
				System.out.println(" ... and I've seen this transition...");
				return false;
			}
			else {
				nextstates.add(stateid);
				System.out.println(" ... recording new transition " + previousState + " --> " + stateid);
				return true;
			}
		} else {
			// never seen this action (or transition)
			LinkedList<Integer> nextstates = new LinkedList<Integer>();
			nextstates.add(stateid);
			action_state_map.put(actionid, nextstates);
			System.out.println("Recording new transition " + previousState + " --> " + stateid);
			return true;
		}

	}
	void printEverythingIKnow() {
		// got all the states, print out what we know....

		System.out.println(" +++++++ Statemap contains all " + state_id_map.size() + " ids +++++++++ ");
		for( int i = 0; i < state_id_map.size(); i++ ) {
			System.out.println( "State " + i);
			Set<String> st = id_state_map.get(i);
			for( String s: st) { System.out.println( "  " + s); }
		}
		System.out.println(" +++++++ Action map contains " + action_id_map.size() + " ids +++++++++ ");
		for( int i = 100; i < 100+action_id_map.size(); i++ ) {
			System.out.println( "Action " + i);
			Set<String> st = id_action_map.get(i);
			for( String s: st) { System.out.println( "  " + s); }
		}
		System.out.println(" +++++++ Observed Transitions +++++++++ ");
		for ( Integer s0 : transition_map.keySet() ) {
			System.out.println(s0 + " -->");
			HashMap<Integer, LinkedList<Integer>> action_state = transition_map.get(s0);
			for ( Integer a0 : action_state.keySet() ) {
				System.out.print( "  " + a0 + " --> ");
				for (Integer ns : action_state.get(a0)) {
					System.out.print( ns + " ");
				}
				System.out.print("\n");
			}
		}

	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println("Command:");
			String command = s.next();
			if (command.equals("quit")) break;
			if (command.equals("step")) {
				k.RunAllTilOutput();
			}
		}
	}
}


/**
 * The WMEFilter filters out wmes from a string obtained via a call to
 * the kernel such as "print --depth 3 --internal i2"
 * 
 * NOTE that the --internal flag is REQUIRED.
 * 
 * Typical usage is this:
 *   Create a subclass that implements _firstPassFilter and _secondPassFilter
 *   call runFilter() on an instance of that subclass to build a state representation
 *
 */
abstract class WMEFilter {
	
	// BUG: this pattern won't allow whitespace in attributes/values
	static Pattern p = Pattern.compile("\\((\\d+): (\\w+) \\^(\\S+ \\S+)\\)");
	TreeSet<String> rootsICareAbout = new TreeSet<String>();
	TreeSet<String> rootsIWillIgnore = new TreeSet<String>();

	public Set<String> runFilter(String internalWMEString) {
		firstPassFilterSourceIds(internalWMEString);
		return secondPassBuildRepresentation(internalWMEString);	
	}
	
	abstract void firstPassFilter( String tt, String root, String attr_val);
	/**
	 * On the first pass through the wmes, we just want to figure out
	 * which IDs we care about... those will be the ones that are in 
	 * rootsICareAbout and NOT in rootsIWillIgnore
	 * 
	 * @param internalWMEString
	 * @return
	 */
	private void firstPassFilterSourceIds(String internalWMEString) {
		String tt, root, attr_val;
		Matcher m = p.matcher(internalWMEString);
		
		int lastMatch = 0;
		while( m.find(lastMatch) ) {
			tt = m.group(1);
			root = m.group(2);
			attr_val = m.group(3);
			lastMatch = m.end();
			
			firstPassFilter(tt, root, attr_val);
		}
	}
	
	abstract String secondPassFilter(String tt, String root, String attr_val);
	
	/**
	 * On the second pass, we'll collect wmes from the internalWMEString to
	 * store in the state.
	 * 
	 * @param internalWMEString
	 * @return an unmodifiable set representing the state
	 */
	private Set<String> secondPassBuildRepresentation(String internalWMEString) {
		
		HashSet<String> state = new HashSet<String>(40);
		Matcher m = p.matcher(internalWMEString);
		String tt, root, attr_val, component;
		int lastMatch = 0;
		
		while( m.find(lastMatch) ) {
			tt = m.group(1);
			root = m.group(2);
			attr_val = m.group(3);
			lastMatch = m.end();
			// generally this will be a problem since identifiers may change
			// in ToH, I think it's ok
			
			component = secondPassFilter( tt, root, attr_val );
			if (component != null) state.add( component );
		}
		return Collections.unmodifiableSet(state);
	}
}

/**
 * A WMEFilter for ToH
 * 
 * NOTE that we're purposely not using a complete state representation
 * as we want to observe a lot of 'repeat' states. This is 
 * good for debugging, but not for really learning anything useful.
 * 
 */
class ToHStateWMEFilter extends WMEFilter {

	@Override
	void firstPassFilter(String tt, String root, String attr_val)
	{
		if (root.startsWith("I2") && attr_val.contains("previous-state"))
			System.out.println( " AGENT Input Link shows 'previous state' as " + attr_val);
		
		if (root.startsWith("H")) {
			// only going to care about "holds" for now
			if (attr_val.contains("1") || attr_val.contains("2")){
				rootsICareAbout.add(root);
			}
		} //else System.out.println("Ignoring " + root + " " + attr_val);
	}

	@Override
	String secondPassFilter(String tt, String root, String attr_val) {
		// TODO Auto-generated method stub
		if (rootsICareAbout.contains(root) && !attr_val.contains("above")) {
			// simplify the state representation by only storing the "holds" objects
			// and removing the 'above' edge....
			return root + " " + attr_val;
		}
		return null;
	}		
	
}

class OutputCommandWMEFilter extends WMEFilter {

	@Override
	void firstPassFilter(String tt, String root, String attr_val) {
		if (attr_val.contains("complete")) {
			System.err.println("Unexpected ... seeing a command with status complete....");
		}
	}

	@Override
	String secondPassFilter(String tt, String root, String attr_val) {
		return attr_val;
	}
	
}