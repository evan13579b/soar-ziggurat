package edu.umich.soar.toh;

import java.util.List;
import java.util.Scanner;

public class TowersOfHanoiCLI {

	public static void main(String [] args) {
        int towerCount = 3;
        int diskCount  = 6;
        
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-t")) {
                if (++i < args.length) {
                    try {
                        towerCount = Integer.parseInt(args[i]);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Bad number for tower count");
                        System.exit(1);
                    }
                } else {
                    System.out.println("No number given for tower count");
                    System.exit(1);
                }
            } else if (args[i].equals("-d")) {
                if (++i < args.length) {
                    try {
                        diskCount = Integer.parseInt(args[i]);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Bad number for disk count");
                        System.exit(1);
                    }
                } else {
                    System.out.println("No number given for disk count");
                    System.exit(1);
                }
            } else {
                System.out.print("Unrecognized option ");
                System.out.println(args[i]);
                System.exit(1);
            }
        }

        Game g = new Game(towerCount, diskCount);
        Scanner scan = new Scanner(System.in);
        boolean userWantsToContinue = true;
        
        while (!g.isAtGoalState() && userWantsToContinue) {
        	display(g.getTowers());
        	
        	while( true ) {
        		System.out.println("Ready>");
        		String command = scan.next();
        		if (command.equals("q") || command.equals("quit")) {
        			userWantsToContinue = false;
        			break;
        		}
        		if (command.equals("s") || command.equals("step")) {
        			g.step();
        			break;
        		}
        		System.out.println("I don't know how to '" + command + "'");
        	}
        	
        }
        // Explicitly calling System.exit() ensures the javaw process shuts down cleanly.
        // Without this we sometimes have a problem with threads not shutting down on their own.
        // We still need to investigate this more fully (it's an SML-java issue when you run
        // the environment through a remote debugger--something's a little off).
        System.exit(0) ;
    }
	
	public static void display(List<Tower> towers) {
		int maxHeight = 0;
		String [][]b = new String[towers.size()][];
		int i = 0;
		for (Tower t: towers) {
			b[i] = t.display();
			if (maxHeight < t.getHeight()) maxHeight = t.getHeight();
			i++;
		}
		
		for (i = maxHeight-1; i >= 0; i--) {
			for (int j = 0; j < towers.size(); j++) {
				if (b[j].length > i){ 
					System.out.print("\t" + b[j][i]);
				}else
					System.out.print("\t ");
			}
			System.out.println("");
		}
		for (int j = 0; j < towers.size(); j++) {
				System.out.print("\t" + "_");
		}
		System.out.println("");
	}
}
