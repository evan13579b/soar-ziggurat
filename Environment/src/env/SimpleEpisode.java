package env;


public class SimpleEpisode implements Episode {
	String action;
	String observation;
	
	public SimpleEpisode(String action,String observation){
		this.action=action;
		this.observation=observation;
	}
	
	public String getAction(){
		return action;
	}
	
	public String getObservation(){
		return observation;
	}
	
	public boolean equals(Object other){
		if(other==null){
			return false;
		}
		else if(!(other instanceof SimpleEpisode)){
			return false;
		}
		else{
			SimpleEpisode otherEpisode=(SimpleEpisode)other;
			return this.action.equals(otherEpisode.action) && this.observation.equals(otherEpisode.observation);
		}
	}
	
	public String toString(){
		return action+observation;
	}
	
	public int hashCode(){
		return action.hashCode()+observation.hashCode();
	}
}
