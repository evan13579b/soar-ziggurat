package env;

public interface Episode {

	String getAction();
	String getObservation();
}
