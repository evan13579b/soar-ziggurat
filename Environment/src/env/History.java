package env;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;


public class History {
	
	
	static public void printHistory(ArrayList<? extends Episode> history){
		for(int i=0;i<history.size();i++){
			System.out.print(history.get(i));
			System.out.print(",");
		}
		System.out.println("");
	}
	
	static public ArrayList<SimpleEpisode> generateHistory(Environment environment,int n,int seed){
		Random random=new Random(seed);
		
		ArrayList<SimpleEpisode> history=new ArrayList<SimpleEpisode>();
		
		environment.setRandomState(random);
		
		String currentAction;
		String currentObservation;
		SimpleEpisode newEpisode;
		for(int i=0;i<n;i++){
			currentAction=environment.getRandomAction(random);
			currentObservation=environment.takeAction(currentAction);

			newEpisode=new SimpleEpisode(currentAction,currentObservation);
			history.add(newEpisode);
		}
		
		return history;
	}
}
