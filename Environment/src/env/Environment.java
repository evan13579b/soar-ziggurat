package env;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class Environment {
	static public final Environment FLIP=new Environment("FLIP",new HashMap<String,HashMap<String,Result>>(){{
		put("0",new HashMap<String,Result>(){{
			put("U",new Result("0","0"));
			put("L",new Result("0","0"));
			put("R",new Result("1","1"));
		}});
		put("1",new HashMap<String,Result>(){{
			put("U",new Result("0","1"));
			put("L",new Result("1","0"));
			put("R",new Result("0","1"));
		}});
	}});
	
	static public final Environment FIGURE_8=new Environment("FIGURE_8",new HashMap<String,HashMap<String,Result>>(){{
		put("0",new HashMap<String,Result>(){{
			put("U",new Result("0","0"));
			put("L",new Result("0","0"));
			put("R",new Result("1","1"));
		}});
		put("1",new HashMap<String,Result>(){{
			put("U",new Result("0","1"));
			put("L",new Result("1","0"));
			put("R",new Result("1","2"));
		}});
		put("2",new HashMap<String,Result>(){{
			put("U",new Result("0","2"));
			put("L",new Result("1","1"));
			put("R",new Result("0","2"));
		}});
	}});
	
	static public final Environment FLIP_TO_THE_THIRD=new Environment("FLIP_TO_THE_THIRD",new HashMap<String,HashMap<String,Result>>(){{
		put("0",new HashMap<String,Result>(){{
			put("U",new Result("0","0"));
			put("L",new Result("0","0"));
			put("R",new Result("1","1"));
		}});
		put("1",new HashMap<String,Result>(){{
			put("U",new Result("0","1"));
			put("L",new Result("1","0"));
			put("R",new Result("1","2"));
		}});
		put("2",new HashMap<String,Result>(){{
			put("U",new Result("0","2"));
			put("L",new Result("1","1"));
			put("R",new Result("1","3"));
		}});
		put("3",new HashMap<String,Result>(){{
			put("U",new Result("0","3"));
			put("L",new Result("1","2"));
			put("R",new Result("0","3"));
		}});
	}});
	
	static public final Environment SQUARE=new Environment("SQUARE",new HashMap<String,HashMap<String,Result>>(){{
		put("0",new HashMap<String,Result>(){{
			put("U",new Result("0","0"));
			put("L",new Result("0","0"));
			put("R",new Result("1","1"));
			put("D",new Result("1","3"));
		}});
		put("1",new HashMap<String,Result>(){{
			put("U",new Result("0","1"));
			put("L",new Result("1","0"));
			put("R",new Result("0","1"));
			put("D",new Result("1","2"));
		}});
		put("2",new HashMap<String,Result>(){{
			put("U",new Result("1","1"));
			put("L",new Result("1","3"));
			put("R",new Result("0","2"));
			put("D",new Result("0","2"));
		}});
		put("3",new HashMap<String,Result>(){{
			put("U",new Result("1","0"));
			put("L",new Result("0","3"));
			put("R",new Result("1","2"));
			put("D",new Result("0","3"));
		}});
	}});
	
	private HashMap<String,HashMap<String,Result>> stateActionResultMap;
	private String state;
	private String name;
	
	public Environment(String name,HashMap<String,HashMap<String,Result>> stateActionResultMap){
		this.stateActionResultMap=stateActionResultMap;
		ArrayList<String> states=new ArrayList<String>(getStates());
		this.state=states.get(0);
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	private Set<String> getStates(){
		return stateActionResultMap.keySet();
	}
	
	private void setState(String state){
		this.state=state;
	}
	
	public Set<String> getAvailableActions(){
		return stateActionResultMap.get(this.state).keySet();
	}
	
	public String takeAction(String action){
		Result result=stateActionResultMap.get(this.state).get(action);
		this.state=result.state;
		
		return result.observation;
	}
	
	public void setRandomState(Random random){
		ArrayList<String> list=new ArrayList<String>(this.getStates());
		this.state=list.get(random.nextInt(list.size()));
	}
	
	public String getRandomAction(Random random){
		ArrayList<String> list=new ArrayList<String>(this.getAvailableActions());
		return list.get(random.nextInt(list.size()));
	}
}
