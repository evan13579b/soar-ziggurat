package lst;


import java.util.ArrayList;
import java.util.HashMap;

import env.Episode;
import env.History;

public class LSTNode<E extends Episode> {
	public static final int spacesPerLevel=4;
	private static int nextID = 0;
	
	E episode;
	LSTNode<E> parent;
	int level;
	ArrayList<LSTNode<E>> children=new ArrayList<LSTNode<E>>();
	HashMap<String,String> transitionMap=null;
	LSTNode<E> loopTarget;
	private int id;
	
	public LSTNode(E episode,LSTNode<E> parent){
		this.episode=episode;
		this.parent=parent;
		if(parent==null){
			this.level=0;
		}
		else{
			this.level=parent.level+1;
		}
		id = nextID;
		nextID++;
	}
	public int getID() { return id; }
	public static int getLastKnownID() { return nextID-1; }
	
	public void setLoopTarget(LSTNode<E> loopTarget){
		this.loopTarget=loopTarget;
	}
	
	public LSTNode<E> getLoopTarget(){
		return loopTarget;
	}
	
	public void printSpaces(int n){
		for(int i=0;i<n;i++){
			System.out.print(" ");
		}
	}
	
	public LSTNode<E> getParent(){
		return parent;
	}
	
	public void printTreeInOrder(int level,boolean verbose){
		System.out.print("lstPrint: ");
		printSpaces(LSTNode.spacesPerLevel*level);
		if(this.episode==null){
			System.out.println("root");
		}
		else{
			System.out.println(episode);
		}

		if (loopTarget!=null){
			System.out.print("lstPrint: ");
			printSpaces(LSTNode.spacesPerLevel * (level + 1));
			if (loopTarget.getEpisode() == null) {
				System.out.println("loop to root");
			} else {
				System.out.print("loop to ");History.printHistory(loopTarget.getEpisodesToRoot());
				History.printHistory(loopTarget.getEpisodesToRoot());
			}
		}
		else if (!children.isEmpty()) {
			for (LSTNode<E> child : children) {
				child.printTreeInOrder(level + 1,verbose);
				
			}
		}
		else if(transitionMap!=null){
			printTransitionMap(level+1);
		}
	}
	
	public void printTransitionMap(int level){
		for(String action:this.transitionMap.keySet()){
			System.out.print("lstPrint: ");
			printSpaces(LSTNode.spacesPerLevel*level);
			System.out.println(action+" --> "+this.transitionMap.get(action));
		}
	}
	
	public ArrayList<LSTNode<E>> getChildren(){
		return children;
	}
	
	public void setChildren(ArrayList<LSTNode<E>> children){
		this.children=children;
	}
	
	public E getEpisode(){
		return episode;
	}
	
	public void setTransitionMap(HashMap<String,String> transitionMap){
		this.transitionMap=transitionMap;
	}
	
	public HashMap<String,String> getTransitionMap(){
		return this.transitionMap;
	}
	
	public ArrayList<LSTNode<E>> getAncestorsInOrder(){
		ArrayList<LSTNode<E>> ancestors=new ArrayList<LSTNode<E>>();
		LSTNode<E> currentNode=this;
		while(currentNode.parent!=null){
			ancestors.add(currentNode.parent);
			currentNode=currentNode.parent;
		}
		return ancestors;
	}
	public String rootedString() {
		StringBuffer sb = new StringBuffer(40);
		LSTNode<E> current = this;
		while(current != null) {
			sb.append(current.toString());
			sb.append('-');
			current = current.parent;
		}
		sb.setLength(sb.length()-1);
		return sb.toString();
	}
	public ArrayList<E> getEpisodesToRoot(){
		ArrayList<E> episodesToRoot=new ArrayList<E>();
		LSTNode<E> currentNode=this;
		while(currentNode.parent!=null){
			episodesToRoot.add(currentNode.episode);
			currentNode=currentNode.parent;
		}
		return episodesToRoot;
	}
	
	public String toString(){
		if(episode==null){
			return "root";
		}
		else{
			return episode.toString();
		}
	}
}
