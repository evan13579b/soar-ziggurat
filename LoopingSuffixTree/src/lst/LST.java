package lst;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;

import env.Environment;
import env.Episode;
import env.History;
import env.SimpleEpisode;

public class LST<E extends Episode> {
	public LSTNode<E> rootNode=null;
	
	static public void main(String[] Argv){
		Environment environment=Environment.FIGURE_8;
		
		Random random=new Random();
		
		ArrayList<SimpleEpisode> trainHistory=History.generateHistory(environment,10000,random.nextInt());		
		ArrayList<SimpleEpisode> testHistory=History.generateHistory(environment,1000,random.nextInt());

		
		boolean verbose=false;
		//boolean verbose=true;
		
		//boolean approximate=true;
		String algorithm="alg3EvanInterpretation";
		
		int maxPrefixSize=3;
		
		//if(verbose){
		//	System.out.print("main: The history is ");History.printHistory(trainHistory);
		//}
		
		LST<SimpleEpisode> lst=new LST<SimpleEpisode>(trainHistory,maxPrefixSize,algorithm,verbose);
		lst.print(verbose);
		System.out.println("Environment Name: "+environment.getName());
		lst.findAccuracy(testHistory, false);
	}
	
	public void print(boolean verbose){
		rootNode.printTreeInOrder(0,verbose);
	}
	
	/**
	 * Create a .dot file for Graphviz to layout the LST
	 * 
	 * @param filename
	 */
	public void createDotFile(String filename) {
		// Baf! This is ugly ;)
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			PrintStream ps = new PrintStream(fos);
			ps.println("digraph LST {");
			Stack<LSTNode<E>> stack = new Stack<LSTNode<E>>();
			stack.push(rootNode);
			while( !stack.isEmpty() ) {
				LSTNode<E> n = stack.pop();
				LSTNode<E> p = n.getParent();
				if (p == null) {
					ps.println("\"" + n.getID() + "\" [shape=circle, label=\"null\"];");
				}
				else {
					if((n.getChildren() != null && n.getChildren().size() > 0) || n.getLoopTarget() != null) {
						ps.println("\"" + n.getID() + "\" [shape=circle, label=\"" + 
								String.valueOf(n.getEpisode()) + "\"];");
					}
					else if (n.getTransitionMap() != null)  {
						StringBuffer sb = new StringBuffer(50);
						sb.append('"');
						sb.append(n.getID());
						sb.append('"');
						sb.append(" [shape=none, label=< <table border=\"0\">");
						sb.append("<tr><td cellpadding=\"5\"><b>");
						sb.append(String.valueOf(n.getEpisode()));
						sb.append("</b></td></tr><hr />");
						sb.append("<tr><td>");
						for (String k : n.getTransitionMap().keySet()) {
							sb.append(k);
							sb.append(" : ");
							sb.append(n.getTransitionMap().get(k));
							sb.append("<br />\n");
						}
						sb.append("</td></tr>\n");
						sb.append("</table>>];");
						ps.println(sb.toString());			

					}
					ps.println("\"" + p.getID() + "\"->\"" + n.getID() + "\"");
					if (n.getLoopTarget() != null) {
						ps.println("\"" + n.getID() + "\"->\"" + n.getLoopTarget().getID() + "\"");
					}
				}
				if (n.getChildren() != null) {
					stack.addAll(n.getChildren());
				}
			}
			ps.println("labelloc=\"t\";");
			ps.println("label=\"" + filename + "\";");
			ps.println("}");
			ps.close();
		} catch (IOException e) {
			e.printStackTrace();

		}

	}
	
	public LST(ArrayList<E> history,int maxPrefixSize,String algorithm,boolean verbose){
		if(verbose)System.out.println("algorithm:"+algorithm);
		
		if(algorithm.equals("alg3WallaceInterpretation")){
			createNonLoopingSuffixTree(history,verbose);
			loopifySuffixTree(history,verbose);
		}
		else if(algorithm.equals("alg2")){
			createLST(history,maxPrefixSize,false,verbose);
		}
		else if (algorithm.equals("alg3EvanInterpretation")) {
			createLST(history,maxPrefixSize,true,verbose);
		}
		else{
			System.out.println("Algorithm "+algorithm+" not recognized.");
		}
	}
	
	private void loopifySuffixTree(ArrayList<E> history,boolean verbose){
		if(verbose)System.out.println("loopifySuffixTree");
		
		LinkedList<LSTNode<E>> expansionQueue=new LinkedList<LSTNode<E>>();
		expansionQueue.addFirst(this.rootNode);
		
		LSTNode<E> currentNode;
		while(!expansionQueue.isEmpty()){
			if(verbose){
				print(false);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			currentNode=expansionQueue.removeLast();
			
			loopToHighestPossible(currentNode,history,verbose);
			if(currentNode.loopTarget==null){
				for(LSTNode<E> child:currentNode.getChildren()){
					expansionQueue.addFirst(child);
				}
			}
		}
	}
	
	private void loopToHighestPossible(LSTNode<E> currentNode,ArrayList<E> history,boolean verbose){
		if(verbose)System.out.println("loopToHighestPossible");
		
		ArrayList<LSTNode<E>> ancestorsInOrder=currentNode.getAncestorsInOrder();
		
		LSTNode<E> currentAncestor;
		for(int i=ancestorsInOrder.size()-1;i>=0;i--){
			currentAncestor=ancestorsInOrder.get(i);
			if(tryLoop(currentNode,currentAncestor,history,verbose)){
				return;
			}
		}
	}
	
	/**
	 * Simulates the current tree and the tree that has the proposed loop side by side by testing predictions before and after removing the loop.
	 * @param currentNode
	 * @return
	 */
	private boolean tryLoop(LSTNode<E> node,LSTNode<E> ancestor,ArrayList<E> history,boolean verbose){
		if(verbose)System.out.println("tryLoop");
		
		ArrayList<E> simHistory=new ArrayList<E>();
		
		if(verbose){
			node.setLoopTarget(ancestor);
			print(false);
			node.setLoopTarget(null);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}	
		
		E currentEpisode;
		String currentAction,currentResult;
		int historyReadWithoutLoop; int historyReadWithLoop;
		for(int i=0;i<history.size();i++){
			currentEpisode=history.get(i);
			currentAction=currentEpisode.getAction();
			currentResult=currentEpisode.getObservation();
			
			historyReadWithoutLoop=historyReadForPrediction(currentAction,currentResult,simHistory,verbose);
			if(historyReadWithoutLoop!=-1){
				if(verbose)System.out.println("read without loop:"+historyReadWithoutLoop);
				//History.printHistory(simHistory);
				node.setLoopTarget(ancestor);
				//print(false);

				historyReadWithLoop=historyReadForPrediction(currentAction,currentResult,simHistory,verbose);
				node.setLoopTarget(null);
				if(/*historyReadWithoutLoop*/-1==historyReadWithLoop){
					if(verbose){
						System.out.println("read with loop:"+historyReadWithLoop);
						History.printHistory(simHistory);
					}
					
					return false;
				}
				else{
					if(verbose)System.out.println("read with loop:"+historyReadWithLoop);
					//History.printHistory(simHistory);
				}
			}
			else{
				if(verbose)System.out.println("read without loop:"+historyReadWithoutLoop);
				//History.printHistory(simHistory);
			}

			simHistory.add(currentEpisode);
		}
		
		node.setLoopTarget(ancestor);
		return true;
	}
	
	private int historyReadForPrediction(String action,String result,ArrayList<E> history,boolean verbose){
		//if(verbose)System.out.println("historyReadForPrediction");
		
		E currentEpisode; LSTNode<E> currentNode=this.rootNode;
		
		ArrayList<LSTNode<E>> children;
		for(int i=history.size()-1;i>=0;i--){ 
			currentEpisode=history.get(i); 
			//System.out.println("predict: currentNode:"+currentNode);
			//System.out.println("predict: currentEpisode:"+currentEpisode);
			children=currentNode.getChildren(); 
		    if(currentNode.getTransitionMap()!=null && currentNode.getLoopTarget()==null){ 
		    	//System.out.println(currentNode.getTransitionMap());
		    	//System.out.println(currentNode.getTransitionMap().get(action));
		    	if(currentNode.getTransitionMap().containsKey(action) && currentNode.getTransitionMap().get(action).equals(result)){
		    		if(verbose)System.out.println("action:"+action+",result:"+result+",predicted:"+currentNode.getTransitionMap().get(action));
		    		return history.size()-i-1;
		    	}
		    	else{
		    		if(verbose)System.out.println("action:"+action+",result:"+result+",predicted:"+currentNode.getTransitionMap().get(action));
		    		return -1;
		    	}
			}
			else if(children.isEmpty() && currentNode.getLoopTarget()==null){ 
				if(verbose)System.out.println("action:"+action+",result:"+result+",predicted:"+currentNode.getTransitionMap().get(action));
				return -1;
			}
			else if(currentNode.getLoopTarget()!=null){ 
				currentNode=currentNode.getLoopTarget();
				i++;
			}
			else{
				boolean currentNodeChanged=false;
				for(LSTNode<E> child:children){
					if(child.getEpisode().equals(currentEpisode)){
						currentNode=child;
						currentNodeChanged=true;
						break;
					}
				}
				if(!currentNodeChanged){
					if(verbose)System.out.println("action:"+action+",result:"+result+",predicted:"+currentNode.getTransitionMap().get(action));
					return -1;
				}
			}
		}
		
		if(verbose)System.out.println("out of history");
		return -1;
	}
	
	private void createNonLoopingSuffixTree(ArrayList<E> history,boolean verbose){
		LinkedList<LSTNode<E>> expansionQueue=new LinkedList<LSTNode<E>>(); // Linked list acts as the expansion queue
		this.rootNode=new LSTNode<E>(null,null); //LSTNode is a node in the looping suffix tree.
		expansionQueue.addFirst(this.rootNode);
		
		LSTNode<E> currentNode;
		HashSet<E> transitionSet;
		while(!expansionQueue.isEmpty()){
			currentNode=expansionQueue.removeLast();
			transitionSet=getTransitionSet(currentNode,history,verbose);
			
			if(isDeterministic(transitionSet)){
				currentNode.setTransitionMap(createTransitionMap(transitionSet));
								
				continue;
			}
			else{
				LSTNode<E> currentExtension;
				ArrayList<LSTNode<E>> currentChildren=currentNode.getChildren();
				for(E currentEpisode:getExtensionSet(history,currentNode)){
					currentExtension=new LSTNode<E>(currentEpisode,currentNode);
					currentChildren.add(currentExtension);
					expansionQueue.addFirst(currentExtension);
				}
			}
		}
	}
	
	/**
	 * This is algorithm 1.
	 * 
	 * @param history
	 * @param verbose
	 */
	private void createLST(ArrayList<E> history,int maxPrefixSize,boolean approximate,boolean verbose){
		LinkedList<LSTNode<E>> expansionQueue=new LinkedList<LSTNode<E>>(); // Linked list acts as the expansion queue
		this.rootNode=new LSTNode<E>(null,null); //LSTNode is a node in the looping suffix tree.
		expansionQueue.addFirst(this.rootNode);
		
		LSTNode<E> currentNode;
		HashSet<E> transitionSet;
		while(!expansionQueue.isEmpty()){
			currentNode=expansionQueue.removeLast();
			transitionSet=getTransitionSet(currentNode,history,verbose);
			
			if(verbose){
				System.out.print("createLST: The transition set for "+currentNode+" is ");History.printHistory(new ArrayList<E>(transitionSet));
				if(isDeterministic(transitionSet)){
					System.out.println("createLST: This transition set is deterministic.");
				}
				else{
					System.out.println("createLST: This transition set is not deterministic.");
				}
			}
			
			LSTNode<E> ancestor;
			if(isDeterministic(transitionSet)){
				currentNode.setTransitionMap(createTransitionMap(transitionSet));
				
				if(verbose)System.out.println("createLST: "+currentNode.getEpisode()+" is deterministic");
				
				continue;
			}
			else{
				if(approximate){
					ancestor=getHighestLoopAncestorAlg3(currentNode,history,verbose);
				}
				else{
					ancestor=getHighestLoopAncestorAlg2(currentNode,history,maxPrefixSize,verbose);
				}
				
				
				if(ancestor!=null){
					currentNode.setLoopTarget(ancestor); // add the loop
					if(verbose)System.out.println("createLST: "+currentNode.getEpisode()+" is looping back to "+ancestor.getEpisode());
				}
				else{
					LSTNode<E> currentExtension;
					ArrayList<LSTNode<E>> currentChildren=currentNode.getChildren();
					for(E currentEpisode:getExtensionSet(history,currentNode)){
						currentExtension=new LSTNode<E>(currentEpisode,currentNode);
						currentChildren.add(currentExtension);
						expansionQueue.addFirst(currentExtension);
					}
					if(verbose)System.out.println("createLST: "+currentNode.getEpisode()+" is adding children");
				}
			}
		}
	}
	
	
	/**
	 * @param currentNode
	 * @param history
	 * @param verbose
	 * @return
	 */
	private HashSet<E> getTransitionSet(LSTNode<E> currentNode,ArrayList<E> history,boolean verbose){
		HashSet<E> transitionSet=new HashSet<E>();
		
		if(currentNode==this.rootNode){
			transitionSet.addAll(history);
		}
		else{
			ArrayList<E> episodesToRoot=currentNode.getEpisodesToRoot(); // get a list of episodes to the root because this node represents the list of episodes to the root not just the episode in the current node
			for(int i=0;i<history.size()-1;i++){ // move back along the history
				for(int j=0;j<episodesToRoot.size();j++){ // at each point i in the history check this list of episodes represented by this node starting at the first episode which is (episodesToRoot.size()+1) nodes deep in the history and ending at i
					
					if(i-episodesToRoot.size()+1<0 || !episodesToRoot.get(j).equals(history.get(i-episodesToRoot.size()+1+j))){
						break; // if the index we are checking is out of bounds || one of the episodes does not match break out of this inner loop
					}
					else if(j==episodesToRoot.size()-1){ // right before the loop is done running we must have never seen unequal episodes so whatever occurs after point i in the history (which is the last episode in the list of episodes represented by the current node) must a transition after it
						transitionSet.add(history.get(i+1));
					}
				}
			}
		}
		
		return transitionSet;
	}
	
	/**
	 * This one is pretty simple, just make sure that there is only one of each action in this set of transitions.
	 * 
	 * @param transitionSet
	 * @return
	 */
	private boolean isDeterministic(HashSet<E> transitionSet){
		HashSet<String> actionSet=new HashSet<String>();
		String currentAction;
		for(E episode:transitionSet){
			currentAction=episode.getAction();
			if(actionSet.contains(currentAction)){
				return false;
			}
			actionSet.add(currentAction);
		}
		return true;
	}
	
	/**
	 * This is basically isLoopable but I figured theres no reason to have it check for loopability in any of the ancestors and then find
	 * the highest loopable ancestor in a different function. Returns null if the currentNode is not loopable.
	 * 
	 * @param currentNode
	 * @param history
	 * @param verbose
	 * @return
	 */
	private LSTNode<E> getHighestLoopAncestorAlg3(LSTNode<E> currentNode,ArrayList<E> history,boolean verbose){
		ArrayList<LSTNode<E>> ancestorsInOrder=currentNode.getAncestorsInOrder();
		//print(verbose);
		int baselineCorrect=simulate(history,verbose); // Get the number correct before adding the loop.
		
		if(verbose){
			System.out.println("ancestor: ---------------------");
			System.out.println("ancestor: currentNode="+currentNode);
			System.out.println("ancestor: baseline="+baselineCorrect);
		}
			
		LSTNode<E> bestAncestor=null;
		int bestCorrect=baselineCorrect;
		LSTNode<E> currentAncestor;
		for(int i=ancestorsInOrder.size()-1;i>=0;i--){
			currentAncestor=ancestorsInOrder.get(i);
			
			int simulatedCorrect=getSimulatedLoopAccuracy(currentNode,currentAncestor,history,verbose);
			
			if(verbose){
				System.out.println("ancestor: ancestor="+currentAncestor);
				System.out.println("ancestor: simulatedCorrect="+simulatedCorrect);
			}
				
			if(simulatedCorrect>bestCorrect){
				return currentAncestor;
				//bestAncestor=currentAncestor; // I've done this a few different ways that don't seem to make much of a difference so far.
				//bestCorrect=simulatedCorrect; // Using this highest or the best didn't change anything so far and I've often tried both to see if one works better than the other. Maybe it makes a difference in circumstances not yet tested/encountered.
			}
		}
		return bestAncestor;
	}
	
	private int getSimulatedLoopAccuracy(LSTNode<E> currentNode,LSTNode<E> ancestor,ArrayList<E> history,boolean verbose){
		currentNode.setLoopTarget(ancestor);
		int newAccuracy=simulate(history,verbose);
		currentNode.setLoopTarget(null);
		return newAccuracy;
	}
	
	public int findAccuracy(ArrayList<E> history,boolean verbose){
		// simHistory is a history that will grow with each iteration of the loop. It is the history that will be looked at to make a prediction at each point in time as it will represent the history available at each point in time.
		ArrayList<E> simHistory=new ArrayList<E>();
		
		E currentEpisode;
		String currentAction,currentResult,predictedResult;
		int correct=0;
		for(int i=0;i<history.size();i++){
			currentEpisode=history.get(i);
			currentAction=currentEpisode.getAction();
			currentResult=currentEpisode.getObservation();
			predictedResult=predictResult(currentAction,simHistory,verbose);
			
			//if(verbose){
			//	System.out.print("simulate: Current history in simulation is ");History.printHistory(simHistory);
			//	System.out.println("simulate: Prediction is "+predictedResult+" while actual result is "+currentResult);
			//}
			if(currentResult.equals(predictedResult)){
				correct++;
			}
			
			simHistory.add(currentEpisode);
		}

		if(verbose){
			System.out.println("emergency: no");
			System.out.println("emergency: correct="+correct);
		}
		
		//if(verbose){
		//	System.out.print("simulate: Simulation of LST on history ");History.printHistory(history);
			System.out.println("Accuracy: "+(float)correct/history.size()+":"+correct+"/"+history.size());
		//}
		
		return correct;
	}
	
	
	
	public int simulate(ArrayList<E> history,boolean verbose){
		// simHistory is a history that will grow with each iteration of the loop. It is the history that will be looked at to make a prediction at each point in time as it will represent the history available at each point in time.
		ArrayList<E> simHistory=new ArrayList<E>();
		
		E currentEpisode;
		String currentAction,currentResult,predictedResult;
		int correct=0;
		for(int i=0;i<history.size();i++){
			currentEpisode=history.get(i);
			currentAction=currentEpisode.getAction();
			currentResult=currentEpisode.getObservation();
			predictedResult=predictResult(currentAction,simHistory,verbose);
			
			//if(verbose){
			//	System.out.print("simulate: Current history in simulation is ");History.printHistory(simHistory);
			//	System.out.println("simulate: Prediction is "+predictedResult+" while actual result is "+currentResult);
			//}
			if(currentResult.equals(predictedResult)){
				correct++;
			}
			else if(predictedResult!=null){
				if(verbose)
					System.out.println("emergency: yes");
				return 0;
			}
			else{
				if(verbose){
				print(verbose);
					System.out.println("emergency: simHistory="+simHistory);
					System.out.println("emergency: action="+currentAction);
					System.out.println("emergency: predicted="+predictedResult);
					System.out.println("emergency: actual="+currentResult);
				}
			}
			
			simHistory.add(currentEpisode);
		}

		if(verbose){
			System.out.println("emergency: no");
			System.out.println("emergency: correct="+correct);
		}
		
		//if(verbose){
		//	System.out.print("simulate: Simulation of LST on history ");History.printHistory(history);
			System.out.println("simulate: "+correct+"/"+history.size()+":"+(float)correct/history.size());
		//}
		
		return correct;
	}
	
	/**
	 * This is perhaps the most messy function. Note that it increments the loop variable outside the initial loop declaration place.
	 * This occures in the "else if(children.size()==1..." block. This block represents the point where we are looping back to the null
	 * node/rootnode and don't want to eat up an episode of memory because we are only "reading" null.
	 * 
	 * @param action
	 * @param history
	 * @param verbose
	 * @return
	 */
	public String predictResult(String action,ArrayList<E> history,boolean verbose){
		E currentEpisode; LSTNode<E> currentNode=this.rootNode;
		ArrayList<LSTNode<E>> children;
		for(int i=history.size()-1;i>=0;i--){ // go back through the history one episode at a time and move through the suffix tree acordingly.
			currentEpisode=history.get(i); // get current episode
			children=currentNode.getChildren(); // see what are the next places to go in the suffix tree
		    if(currentNode.getTransitionMap()!=null){ // if this node has a transition map then we have reached a leaf and found our prediction
		    	if(currentNode.getTransitionMap().containsKey(action)){
		    		return currentNode.getTransitionMap().get(action);
		    	}
		    	else{
		    		return "Transition not found";
		    	}
			}
			else if(currentNode.getLoopTarget()!=null){ // with one child that is a loop (if node1 loops to node2 that does not make node1 node2's parent unlike for a normal child-parent relationship) we need to loop back and increment the index variable to essentially keep it on the current episode for the next iteration.
				currentNode=currentNode.getLoopTarget();
				i++;
			}			
			else if(children.isEmpty()){ // if there is no transition map but also no children we haven't processed this node yet. Return null to state that we have no idea what to predict.
				return null;
			}

			else{
				boolean currentNodeChanged=false;
				//System.out.println("emergency: currentNode="+currentNode);
				for(LSTNode<E> child:children){// go to the child that represents the episode we see
					if(child.getEpisode().equals(currentEpisode)){
						currentNode=child;
						currentNodeChanged=true;
						break;
					}
				}
				if(!currentNodeChanged){
					
					if(verbose)System.out.println("emergency: not node changed");
					return "ChildNotFound";
				}
			}
		}
		
		if(verbose){
			System.out.println("Reached the end of history while trying to make a prediction.");
		}
		
		//System.out.println("emergency: reached the end");
		return null;
	}
	/**
	 * This works just like predictResult, but gives a String representing the 
	 * path from the node to the root.
	 * @param history
	 * @param verbose
	 * @return
	 */
	public String getLSTNode(ArrayList<E> history,boolean verbose){
		E currentEpisode; LSTNode<E> currentNode=this.rootNode;
		ArrayList<LSTNode<E>> children;
		for(int i=history.size()-1;i>=0;i--){ // go back through the history one episode at a time and move through the suffix tree acordingly.
			currentEpisode=history.get(i); // get current episode
			children=currentNode.getChildren(); // see what are the next places to go in the suffix tree
		    if(currentNode.getTransitionMap()!=null){ // if this node has a transition map then we have reached a leaf and found our prediction
				return currentNode.rootedString(); 
			}
			else if(currentNode.getLoopTarget()!=null){ // with one child that is a loop (if node1 loops to node2 that does not make node1 node2's parent unlike for a normal child-parent relationship) we need to loop back and increment the index variable to essentially keep it on the current episode for the next iteration.
				currentNode=currentNode.getLoopTarget();
				i++;
			}			
			else if(children.isEmpty()){ // if there is no transition map but also no children we haven't processed this node yet. Return null to state that we have no idea what to predict.
				return "?";
			}

			else{
				boolean currentNodeChanged=false;
				//System.out.println("emergency: currentNode="+currentNode);
				for(LSTNode<E> child:children){// go to the child that represents the episode we see
					if(child.getEpisode().equals(currentEpisode)){
						currentNode=child;
						currentNodeChanged=true;
						break;
					}
				}
				if(!currentNodeChanged){
					
					if(verbose)System.out.println("emergency: not node changed");
					return "ChildNotFound";
				}
			}
		}
		
		if(verbose){
			System.out.println("Reached the end of history while trying to make a prediction.");
		}
		
		//System.out.println("emergency: reached the end");
		return "??";
	}
	
	private LSTNode<E> getHighestLoopAncestorAlg2(LSTNode<E> currentNode,ArrayList<E> history,int maxPrefixSize,boolean verbose){
		ArrayList<LSTNode<E>> ancestorsInOrder=currentNode.getAncestorsInOrder();
		
		//print(verbose);

		if(verbose){
			System.out.println("ancestor: ---------------------");
			System.out.println("ancestor: currentNode="+currentNode);
		}
			
		LSTNode<E> ancestor;
		for(int i=ancestorsInOrder.size()-1;i>=0;i--){
			ancestor=ancestorsInOrder.get(i);
			
			boolean loopable=checkLoopability(currentNode,ancestor,history,maxPrefixSize,verbose);
			
			if(verbose){
				System.out.println("ancestor: ancestor="+ancestor);
			}
				
			if(loopable){
				return ancestor;
			}
		}
		return null;
	}
	

	private boolean checkLoopability(LSTNode<E> currentNode,LSTNode<E> ancestor,ArrayList<E> history,int maxPrefixSize,boolean verbose){
		ArrayList<ArrayList<E>> currentNodePrefixes = new ArrayList<ArrayList<E>>(1000);
		ArrayList<ArrayList<E>> ancestorPrefixes = new ArrayList<ArrayList<E>>(1000);
		
		for (int i = maxPrefixSize; i <= maxPrefixSize; i++ ) {
			currentNodePrefixes.addAll(getPrefixes(currentNode,history,i,verbose));
			ancestorPrefixes.addAll(getPrefixes(ancestor,history,i,verbose));
		}
		
		boolean matchFound;
		for(ArrayList<E> currentNodePrefix:currentNodePrefixes){
			matchFound=false;
			for(ArrayList<E> ancestorPrefix:ancestorPrefixes){
				if(ancestorPrefix.equals(currentNodePrefix)){
					HashSet<E> currentNodePrefixTransitionSet=getTransitionSetFromPrefix(currentNode,currentNodePrefix,history,verbose);;
					HashSet<E> ancestorPrefixTransitionSet=getTransitionSetFromPrefix(ancestor,ancestorPrefix,history,verbose);

					if(!currentNodePrefixTransitionSet.equals(ancestorPrefixTransitionSet)){
						
						//System.out.println(currentNode);
						//System.out.println("Transition set not equal");
						//History.printHistory(reverse(currentNodePrefix));
						//History.printHistory(reverse(ancestorPrefix));
						//History.printHistory(new ArrayList<Episode>(currentNodePrefixTransitionSet));
						//History.printHistory(new ArrayList<Episode>(ancestorPrefixTransitionSet));
						return false;
					}
					
					matchFound=true;
					break;
				}
			}
			if(!matchFound){
				System.out.println("Non common prefix");
				return false;
			}
			
		}
		
		
		
		return true;
	}
	
	private HashSet<E> getTransitionSetFromPrefix(LSTNode<E> currentNode,ArrayList<E> prefix,ArrayList<E> history,boolean verbose){
		HashSet<E> transitionSet=new HashSet<E>();
		
		ArrayList<E> episodeList=reverse(prefix);
		
		if(currentNode!=this.rootNode){
			ArrayList<E> episodesToRoot=currentNode.getEpisodesToRoot();
			for(E episode:episodesToRoot){
				episodeList.add(episode);
			}
		}
		
		for(int i=0;i<history.size()-1;i++){ // move back along the history
			for(int j=0;j<episodeList.size();j++){ // at each point i in the history check this list of episodes represented by this node starting at the first episode which is (episodesToRoot.size()+1) nodes deep in the history and ending at i
				
				if(i-episodeList.size()+1<0 || !episodeList.get(j).equals(history.get(i-episodeList.size()+1+j))){
					break; // if the index we are checking is out of bounds || one of the episodes does not match break out of this inner loop
				}
				else if(j==episodeList.size()-1){ // right before the loop is done running we must have never seen unequal episodes so whatever occurs after point i in the history (which is the last episode in the list of episodes represented by the current node) must a transition after it
					transitionSet.add(history.get(i+1));
				}
			}
		}
		
		return transitionSet;
	}
	
	private ArrayList<E> reverse(ArrayList<E> list){
		ArrayList<E> reversedList=new ArrayList<E>();
		for(int i=list.size()-1;i>=0;i--){
			reversedList.add(list.get(i));
		}
		
		return reversedList;
	}
	
	private ArrayList<ArrayList<E>> getPrefixes(LSTNode<E> node,ArrayList<E> history,int maxPrefixSize,boolean verbose){
		ArrayList<ArrayList<E>> prefixes=new ArrayList<ArrayList<E>>();
		
		ArrayList<E> prefix;
		for(int i=1;i<=history.size();i++){ 
			// i run all the way to history.size() even though that index is out of bounds because that allows me to 
			// grab the final prefix for null (which is present after the final element)
			// to make up for this i make sure that the history is only indexed at i if i<history.size. The final iteration
			// is then ignored if the episode is not equal to null
			if(node.getEpisode()==null || (i<history.size() && history.get(i).equals(node.getEpisode()))){
				prefix=new ArrayList<E>();
				for(int j=0;i-1-j>0 && j<maxPrefixSize;j++){
					int prefixIndex=i-1-j;
					prefix.add(history.get(prefixIndex));
				}
			
				if(prefix.size()==maxPrefixSize){
					prefixes.add(prefix);
				}
				
			}
		}
		
		
		return prefixes;
	}

	
	
	
	private ArrayList<E> getExtensionSet(ArrayList<E> history,LSTNode<E> currentNode) {
		HashSet<E> extensionSet = new HashSet<E>();

		ArrayList<E> episodesToRoot=currentNode.getEpisodesToRoot();
		
		if (currentNode == this.rootNode) {
			extensionSet.addAll(history);
		} else {
			for (int i = history.size()-1; i>0; i--) {
				for(int j=0;j<episodesToRoot.size();j++){
					int historyIndex=i-episodesToRoot.size()+1+j;
					if (historyIndex<1 || !episodesToRoot.get(j).equals(history.get(historyIndex))) {
						break;
					}
					if(historyIndex==i){
						extensionSet.add(history.get(i-episodesToRoot.size()));
					}
				}
			}
		}

		//System.out.println("extensionSet:        history:"+history);
		 // System.out.print("extensionSet: episodesToRoot:");History.printHistory(episodesToRoot);
		//  System.out.print("extensionSet:   extensionSet:");History.printHistory(new ArrayList<Episode>(extensionSet));
		return new ArrayList<E>(extensionSet);
	}
	
	
	
	
	
	

	
	private HashMap<String,String> createTransitionMap(HashSet<E> transitionSet){
		HashMap<String,String> transitionMap=new HashMap<String,String>();
		String currentAction;
		String currentResult;
		for(E episode:transitionSet){
			currentAction=episode.getAction();
			currentResult=episode.getObservation();

			transitionMap.put(currentAction, currentResult);
		}
		
		return transitionMap;
	}
	


}
